# 2.0.0
Jonas Kalderstam (4):
  * [8d46389] Migrated from jcenter to mavencentral
  * [c5ac144] Added gradle nexus publish for increased automation
  * [5ede858] Renamed the integration test
  * [1e1e296] Replaced AudioContentType with a simple string to fix webm

# 1.1.0
Jonas Kalderstam (2):
  * [1d0af89] Fixed parsing error for low level users

    This is a potentialy breaking change where some fields
    have been made nullable in case no reviews have been
    done yet for example.

# 1.0.0
Jonas Kalderstam (1):
  * [6af6faa] Updated to Kotlin 1.4 and Moshi 1.11

# 0.0.6
Jonas Kalderstam (3):
  * [339ec7e] Added pagination to extension functions
  * [ce15972] Added flow extension functions
  * [4b4b73a] Added a test for flow extension function

# 0.0.5
Jonas Kalderstam (2):
  * [40ecdcc] Added proper pagination support and converted all IDs to Longs

# 0.0.4
Jonas Kalderstam (3):
  * [8225e1d] Updated library for breaking API changes

# 0.0.3
Jonas Kalderstam (3):
  * [1c97883] Fixed artifact id
  * [d55ed01] Change kotlin-stdlib to a compile-only dep and okhttp to api

# 0.0.2

All get-methods have been tested live in this version

Jonas Kalderstam (5):
  * [cba1ade] Updated UserResponse to fix null issue
  * [6b80be1] Updated ResetResponse to take null dataUpdatedAt into account
