package com.nononsenseapps.wanikani.client

import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import org.junit.Ignore
import org.junit.Test
import kotlin.test.assertNotNull


/**
 * In intellij, add VM options to run configuration with:
 *    -DWANIKANI_API_KEY=...
 */
@Ignore
class RealFlowTest : RealWanikaniTest() {
    @Test
    @Ignore
    fun testGetAssignmentsFlow() {
        runBlocking {
            val responses = wanikani.getAssignmentsFlow(passed = true)

            responses.collect { response ->
                assertNotNull(response.dataUpdatedAt)
            }
        }
    }

    @Test
    @Ignore
    fun testGetReviewsFlow() {
        runBlocking {
            val responses = wanikani.getReviewsFlow()

            responses.collect { response ->
                assertNotNull(response.dataUpdatedAt)
            }
        }
    }

    @Test
    @Ignore
    fun testResetsFlow() {
        runBlocking {
            val responses = wanikani.getResetsFlow()

            responses.collect { response ->
                assertNotNull(response.dataUpdatedAt)
            }
        }
    }

    @Test
    @Ignore
    fun testSubjectsFlow() {
        runBlocking {
            val responses = wanikani.getSubjectsFlow()

            responses.collect { response ->
                assertNotNull(response.dataUpdatedAt)
            }
        }
    }

    @Test
    @Ignore
    fun testLevelProgressionsFlow() {
        runBlocking {
            val responses = wanikani.getLevelProgressionsFlow()

            responses.collect { response ->
                assertNotNull(response.dataUpdatedAt)
            }
        }
    }

    @Test
    @Ignore
    fun testReviewStatisticsFlow() {
        runBlocking {
            val responses = wanikani.getReviewStatisticsFlow()

            responses.collect { response ->
                assertNotNull(response.dataUpdatedAt)
            }
        }
    }

    @Test
    @Ignore
    fun testStudyMaterialsFlow() {
        runBlocking {
            val responses = wanikani.getStudyMaterialsFlow()

            responses.collect { response ->
                assertNotNull(response.dataUpdatedAt)
            }
        }
    }

    @Test
    @Ignore
    fun testVoiceActorsFlow() {
        runBlocking {
            val responses = wanikani.getVoiceActorsFlow()

            responses.collect { response ->
                assertNotNull(response.dataUpdatedAt)
            }
        }
    }

    @Test
    @Ignore
    fun testSpacedRepetitionsFlow() {
        runBlocking {
            val responses = wanikani.getSpacedRepetitionSystemsFlow()

            responses.collect { response ->
                assertNotNull(response.dataUpdatedAt)
            }
        }
    }
}
