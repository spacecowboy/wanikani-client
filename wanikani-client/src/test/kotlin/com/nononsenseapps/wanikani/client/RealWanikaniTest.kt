package com.nononsenseapps.wanikani.client

import org.junit.Before

abstract class RealWanikaniTest {
    protected lateinit var wanikani: WaniKaniV2

    @Before
    fun setup() {
        val WANIKANI_API_KEY: String by System.getProperties()
        wanikani = getWaniKaniClient(apiToken = WANIKANI_API_KEY)
    }
}
