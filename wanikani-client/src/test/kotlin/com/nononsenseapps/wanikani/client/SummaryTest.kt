package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.response.ObjectType
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import java.net.URL
import java.time.Instant
import kotlin.test.assertEquals

class SummaryTest : MockedWebServerTest() {
    @Test
    fun testGet() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "object": "report",
                      "url": "https://api.wanikani.com/v2/summary",
                      "data_updated_at": "2018-04-11T21:00:00.000000Z",
                      "data": {
                        "lessons": [
                          {
                            "available_at": "2018-04-11T21:00:00.000000Z",
                            "subject_ids": [
                              25,
                              26
                            ]
                          }
                        ],
                        "next_reviews_at": "2018-04-11T21:00:00.000000Z",
                        "reviews": [
                          {
                            "available_at": "2018-04-11T21:00:00.000000Z",
                            "subject_ids": [
                              21,
                              23,
                              24
                            ]
                          }
                        ]
                      }
                    }
                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.getSummary()
        }

        assertEquals(ObjectType.REPORT, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/summary"), response.url)
        assertEquals(Instant.parse("2018-04-11T21:00:00.000000Z"), response.dataUpdatedAt)

        val summary = response.data

        assertEquals(Instant.parse("2018-04-11T21:00:00.000000Z"), summary.nextReviewsAt)

        val lesson = summary.lessons.first()

        assertEquals(Instant.parse("2018-04-11T21:00:00.000000Z"), lesson.availableAt)
        assertEquals(listOf(25L, 26L), lesson.subjectIds)

        val review = summary.reviews.first()

        assertEquals(Instant.parse("2018-04-11T21:00:00.000000Z"), review.availableAt)
        assertEquals(listOf(21L, 23L, 24L), review.subjectIds)
    }
}
