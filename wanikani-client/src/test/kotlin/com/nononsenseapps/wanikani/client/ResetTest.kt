package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.response.ObjectType
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException
import java.net.URL
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

class ResetTest : MockedWebServerTest() {
    @Test
    fun testGetAllQueryParams() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(404)
        )

        runBlocking {
            assertFailsWith<HttpException> {
                wanikani.getResets(
                    updatedAfter = Instant.EPOCH,
                    ids = arrayOf(1, 2)
                )
            }
        }

        assertEquals(
            "/resets?updated_after=1970-01-01T00%3A00%3A00Z&ids=1%2C2",
            mockWebServer.takeRequest().path
        )
    }

    @Test
    fun testGetAllResets() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "object": "collection",
                      "url": "https://api.wanikani.com/v2/resets",
                      "pages": {
                        "per_page": 500,
                        "next_url": null,
                        "previous_url": null
                      },
                      "total_count": 2,
                      "data_updated_at": "2017-11-29T19:37:03.571377Z",
                      "data": [
                        {
                          "id": 234,
                          "object": "reset",
                          "url": "https://api.wanikani.com/v2/resets/80463006",
                          "data_updated_at": "2017-12-20T00:24:47.048380Z",
                          "data": {
                            "created_at": "2017-12-20T00:03:56.642838Z",
                            "original_level": 42,
                            "target_level": 8,
                            "confirmed_at": "2017-12-19T23:31:18.077268Z"
                          }
                        }
                      ]
                    }
                """.trimIndent())
            }
        )

        val response = runBlocking {
            wanikani.getResets()
        }

        assertEquals(ObjectType.COLLECTION, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/resets"), response.url)
        assertEquals(500, response.pages.perPage)
        assertNull(response.pages.nextURL)
        assertNull(response.pages.previousURL)
        assertEquals(2, response.totalCount)
        assertEquals(Instant.parse("2017-11-29T19:37:03.571377Z"), response.dataUpdatedAt)

        assertEquals(234, response.data.first().id)
        assertEquals(ObjectType.RESET, response.data.first().`object`)
        assertEquals(URL("https://api.wanikani.com/v2/resets/80463006"), response.data.first().url)
        assertEquals(Instant.parse("2017-12-20T00:24:47.048380Z"), response.data.first().dataUpdatedAt)

        val reset = response.data.first().data

        assertEquals(Instant.parse("2017-12-20T00:03:56.642838Z"), reset.createdAt)
        assertEquals(42, reset.originalLevel)
        assertEquals(8, reset.targetLevel)
        assertEquals(Instant.parse("2017-12-19T23:31:18.077268Z"), reset.confirmedAt)
    }

    @Test
    fun testGetReset() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "id": 234,
                      "object": "reset",
                      "url": "https://api.wanikani.com/v2/resets/234",
                      "data_updated_at": "2017-03-30T11:31:20.438432Z",
                      "data": {
                        "created_at": "2017-12-20T00:03:56.642838Z",
                        "original_level": 42,
                        "target_level": 8,
                        "confirmed_at": "2017-12-19T23:31:18.077268Z"
                      }
                    }
                """.trimIndent())
            }
        )

        val response = runBlocking {
            wanikani.getReset(234)
        }

        assertEquals(234, response.id)
        assertEquals(ObjectType.RESET, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/resets/234"), response.url)
        assertEquals(Instant.parse("2017-03-30T11:31:20.438432Z"), response.dataUpdatedAt)

        val reset = response.data

        assertEquals(Instant.parse("2017-12-20T00:03:56.642838Z"), reset.createdAt)
        assertEquals(42, reset.originalLevel)
        assertEquals(8, reset.targetLevel)
        assertEquals(Instant.parse("2017-12-19T23:31:18.077268Z"), reset.confirmedAt)
    }
}
