package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.response.ObjectType
import com.nononsenseapps.wanikani.client.response.StageWithDuration
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import java.net.URL
import java.time.Duration
import java.time.Instant
import kotlin.test.assertEquals

class SpacedRepetitionSystemsTest : MockedWebServerTest() {
    @Test
    fun testGetAll() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "object": "collection",
                      "url": "https://api.wanikani.com/v2/spaced_repetition_systems",
                      "pages": {
                        "per_page": 500,
                        "next_url": null,
                        "previous_url": null
                      },
                      "total_count": 2,
                      "data_updated_at": "2020-06-09T03:38:01.007395Z",
                      "data": [
                        {
                          "id": 1,
                          "object": "spaced_repetition_system",
                          "url": "https://api.wanikani.com/v2/spaced_repetition_systems/1",
                          "data_updated_at": "2020-06-09T03:36:51.134752Z",
                          "data": {
                            "created_at": "2020-05-21T20:46:06.464460Z",
                            "name": "Default system for dictionary subjects",
                            "description": "The original spaced repetition system",
                            "unlocking_stage_position": 0,
                            "starting_stage_position": 1,
                            "passing_stage_position": 5,
                            "burning_stage_position": 9,
                            "stages": [
                              {
                                "interval": null,
                                "position": 0,
                                "interval_unit": null
                              },
                              {
                                "interval": 14400,
                                "position": 1,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 28800,
                                "position": 2,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 82800,
                                "position": 3,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 169200,
                                "position": 4,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 601200,
                                "position": 5,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 1206000,
                                "position": 6,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 2588400,
                                "position": 7,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 10364400,
                                "position": 8,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": null,
                                "position": 9,
                                "interval_unit": null
                              }
                            ]
                          }
                        },
                        {
                          "id": 2,
                          "object": "spaced_repetition_system",
                          "url": "https://api.wanikani.com/v2/spaced_repetition_systems/2",
                          "data_updated_at": "2020-06-09T03:38:01.007395Z",
                          "data": {
                            "created_at": "2020-05-21T20:48:06.470578Z",
                            "name": "Default accelerated system for dictionary subjects",
                            "description": "The original spaced repetition system, but accelerated",
                            "unlocking_stage_position": 0,
                            "starting_stage_position": 1,
                            "passing_stage_position": 5,
                            "burning_stage_position": 9,
                            "stages": [
                              {
                                "interval": null,
                                "position": 0,
                                "interval_unit": null
                              },
                              {
                                "interval": 7200,
                                "position": 1,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 14400,
                                "position": 2,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 28800,
                                "position": 3,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 82800,
                                "position": 4,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 601200,
                                "position": 5,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 1206000,
                                "position": 6,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 2588400,
                                "position": 7,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": 10364400,
                                "position": 8,
                                "interval_unit": "seconds"
                              },
                              {
                                "interval": null,
                                "position": 9,
                                "interval_unit": null
                              }
                            ]
                          }
                        }
                      ]
                    }
                """.trimIndent()
                )
            }
        )

        val responses = runBlocking {
            wanikani.getSpacedRepetitionSystems()
        }

        assertEquals(ObjectType.COLLECTION, responses.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/spaced_repetition_systems"), responses.url)
        assertEquals(Instant.parse("2020-06-09T03:38:01.007395Z"), responses.dataUpdatedAt)

        val response = responses.data.first()

        assertEquals(1, response.id)
        assertEquals(ObjectType.SPACED_REPETITION_SYSTEM, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/spaced_repetition_systems/1"), response.url)
        assertEquals(Instant.parse("2020-06-09T03:36:51.134752Z"), response.dataUpdatedAt)

        val data = response.data

        assertEquals(Instant.parse("2020-05-21T20:46:06.464460Z"), data.createdAt)
        assertEquals("Default system for dictionary subjects", data.name)
        assertEquals("The original spaced repetition system", data.description)
        assertEquals(0, data.unlockingStagePosition)
        assertEquals(1, data.startingStagePosition)
        assertEquals(5, data.passingStagePosition)
        assertEquals(9, data.burningStagePosition)

        assertEquals(
            listOf(
                StageWithDuration(
                    duration = null,
                    position = 0
                ),
                StageWithDuration(
                    duration = Duration.ofSeconds(14400),
                    position = 1
                ),
                StageWithDuration(
                    Duration.ofSeconds(28800),
                    position = 2
                ),
                StageWithDuration(
                    Duration.ofSeconds(82800),
                    position = 3
                ),
                StageWithDuration(
                    Duration.ofSeconds(169200),
                    position = 4
                ),
                StageWithDuration(
                    Duration.ofSeconds(601200),
                    position = 5
                ),
                StageWithDuration(
                    Duration.ofSeconds(1206000),
                    position = 6
                ),
                StageWithDuration(
                    Duration.ofSeconds(2588400),
                    position = 7
                ),
                StageWithDuration(
                    Duration.ofSeconds(10364400),
                    position = 8
                ),
                StageWithDuration(
                    duration = null,
                    position = 9
                )
            ),
            data.stages
        )
    }

    @Test
    fun testGetSpecific() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "id": 1,
                      "object": "spaced_repetition_system",
                      "url": "https://api.wanikani.com/v2/spaced_repetition_systems/1",
                      "data_updated_at": "2020-05-27T16:42:06.705681Z",
                      "data": {
                        "created_at": "2020-05-21T20:46:06.464460Z",
                        "name": "Default system for dictionary subjects",
                        "description": "The original spaced repetition system",
                        "unlocking_stage_position": 0,
                        "starting_stage_position": 1,
                        "passing_stage_position": 5,
                        "burning_stage_position": 9,
                        "stages": [
                          {
                            "interval": null,
                            "position": 0,
                            "interval_unit": null
                          },
                          {
                            "interval": 14400,
                            "position": 1,
                            "interval_unit": "seconds"
                          },
                          {
                            "interval": 28800,
                            "position": 2,
                            "interval_unit": "seconds"
                          },
                          {
                            "interval": 82800,
                            "position": 3,
                            "interval_unit": "seconds"
                          },
                          {
                            "interval": 169200,
                            "position": 4,
                            "interval_unit": "seconds"
                          },
                          {
                            "interval": 601200,
                            "position": 5,
                            "interval_unit": "seconds"
                          },
                          {
                            "interval": 1206000,
                            "position": 6,
                            "interval_unit": "seconds"
                          },
                          {
                            "interval": 2588400,
                            "position": 7,
                            "interval_unit": "seconds"
                          },
                          {
                            "interval": 10364400,
                            "position": 8,
                            "interval_unit": "seconds"
                          },
                          {
                            "interval": null,
                            "position": 9,
                            "interval_unit": null
                          }
                        ]
                      }
                    }
                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.getSpacedRepetitionSystem(1)
        }

        assertEquals(1, response.id)
        assertEquals(ObjectType.SPACED_REPETITION_SYSTEM, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/spaced_repetition_systems/1"), response.url)
        assertEquals(Instant.parse("2020-05-27T16:42:06.705681Z"), response.dataUpdatedAt)

        val data = response.data

        assertEquals(Instant.parse("2020-05-21T20:46:06.464460Z"), data.createdAt)
        assertEquals("Default system for dictionary subjects", data.name)
        assertEquals("The original spaced repetition system", data.description)
        assertEquals(0, data.unlockingStagePosition)
        assertEquals(1, data.startingStagePosition)
        assertEquals(5, data.passingStagePosition)
        assertEquals(9, data.burningStagePosition)

        assertEquals(
            listOf(
                StageWithDuration(
                    duration = null,
                    position = 0
                ),
                StageWithDuration(
                    duration = Duration.ofSeconds(14400),
                    position = 1
                ),
                StageWithDuration(
                    Duration.ofSeconds(28800),
                    position = 2
                ),
                StageWithDuration(
                    Duration.ofSeconds(82800),
                    position = 3
                ),
                StageWithDuration(
                    Duration.ofSeconds(169200),
                    position = 4
                ),
                StageWithDuration(
                    Duration.ofSeconds(601200),
                    position = 5
                ),
                StageWithDuration(
                    Duration.ofSeconds(1206000),
                    position = 6
                ),
                StageWithDuration(
                    Duration.ofSeconds(2588400),
                    position = 7
                ),
                StageWithDuration(
                    Duration.ofSeconds(10364400),
                    position = 8
                ),
                StageWithDuration(
                    duration = null,
                    position = 9
                )
            ),
            data.stages
        )
    }
}
