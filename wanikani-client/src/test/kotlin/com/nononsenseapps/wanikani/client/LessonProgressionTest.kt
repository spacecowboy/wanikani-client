package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.response.ObjectType
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException
import java.net.URL
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

class LessonProgressionTest : MockedWebServerTest() {
    @Test
    fun testGetAllQueryParams() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(404)
        )

        runBlocking {
            assertFailsWith<HttpException> {
                wanikani.getLevelProgressions(
                    updatedAfter = Instant.EPOCH,
                    ids = arrayOf(1, 2)
                )
            }
        }

        assertEquals(
            "/level_progressions?updated_after=1970-01-01T00%3A00%3A00Z&ids=1%2C2",
            mockWebServer.takeRequest().path
        )
    }

    @Test
    fun testGetAllLevelProgressions() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "object": "collection",
                      "url": "https://www.wanikani.com/api/v2/level_progressions",
                      "pages": {
                        "per_page": 500,
                        "next_url": null,
                        "previous_url": null
                      },
                      "total_count": 42,
                      "data_updated_at": "2017-09-21T11:45:01.691388Z",
                      "data": [
                        {
                          "id": 49392,
                          "object": "level_progression",
                          "url": "https://www.wanikani.com/api/v2/level_progressions/49392",
                          "data_updated_at": "2017-03-30T11:31:20.438432Z",
                          "data": {
                            "created_at": "2017-03-30T08:21:51.439918Z",
                            "level": 42,
                            "unlocked_at": "2017-03-30T08:21:51.439918Z",
                            "started_at": "2017-03-30T11:31:20.438432Z",
                            "passed_at": null,
                            "completed_at": null,
                            "abandoned_at": null
                          }
                        }
                      ]
                    }
                """.trimIndent())
            }
        )

        val response = runBlocking {
            wanikani.getLevelProgressions()
        }

        assertEquals(ObjectType.COLLECTION, response.`object`)
        assertEquals(URL("https://www.wanikani.com/api/v2/level_progressions"), response.url)
        assertEquals(500, response.pages.perPage)
        assertNull(response.pages.nextURL)
        assertNull(response.pages.previousURL)
        assertEquals(42, response.totalCount)
        assertEquals(Instant.parse("2017-09-21T11:45:01.691388Z"), response.dataUpdatedAt)

        assertEquals(49392, response.data.first().id)
        assertEquals(ObjectType.LEVEL_PROGRESSION, response.data.first().`object`)
        assertEquals(URL("https://www.wanikani.com/api/v2/level_progressions/49392"), response.data.first().url)
        assertEquals(Instant.parse("2017-03-30T11:31:20.438432Z"), response.data.first().dataUpdatedAt)

        val levelProgression = response.data.first().data

        assertEquals(42, levelProgression.level)
        assertEquals(Instant.parse("2017-03-30T08:21:51.439918Z"), levelProgression.createdAt)
        assertEquals(Instant.parse("2017-03-30T08:21:51.439918Z"), levelProgression.unlockedAt)
        assertEquals(Instant.parse("2017-03-30T11:31:20.438432Z"), levelProgression.startedAt)
        assertNull(levelProgression.passedAt)
        assertNull(levelProgression.completedAt)
        assertNull(levelProgression.abandonedAt)
    }

    @Test
    fun testGetLevelProgression() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "id": 49392,
                      "object": "level_progression",
                      "url": "https://api.wanikani.com/v2/level_progressions/49392",
                      "data_updated_at": "2017-03-30T11:31:20.438432Z",
                      "data": {
                        "created_at": "2017-03-30T08:21:51.439918Z",
                        "level": 42,
                        "unlocked_at": "2017-03-30T08:21:51.439918Z",
                        "started_at": "2017-03-30T11:31:20.438432Z",
                        "passed_at": null,
                        "completed_at": null,
                        "abandoned_at": null
                      }
                    }

                """.trimIndent())
            }
        )

        val response = runBlocking {
            wanikani.getLevelProgression(49392)
        }

        assertEquals(49392, response.id)
        assertEquals(ObjectType.LEVEL_PROGRESSION, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/level_progressions/49392"), response.url)
        assertEquals(Instant.parse("2017-03-30T11:31:20.438432Z"), response.dataUpdatedAt)

        val levelProgression = response.data

        assertEquals(42, levelProgression.level)
        assertEquals(Instant.parse("2017-03-30T08:21:51.439918Z"), levelProgression.createdAt)
        assertEquals(Instant.parse("2017-03-30T08:21:51.439918Z"), levelProgression.unlockedAt)
        assertEquals(Instant.parse("2017-03-30T11:31:20.438432Z"), levelProgression.startedAt)
        assertNull(levelProgression.passedAt)
        assertNull(levelProgression.completedAt)
        assertNull(levelProgression.abandonedAt)
    }
}
