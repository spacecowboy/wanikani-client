package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.response.ObjectType
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException
import java.net.URL
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull
import kotlin.test.assertTrue

class VoiceActorTest : MockedWebServerTest() {
    @Test
    fun testGetAllQueryParams() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(404)
        )

        runBlocking {
            assertFailsWith<HttpException> {
                wanikani.getVoiceActors(
                    updatedAfter = Instant.EPOCH,
                    ids = arrayOf(1, 2)
                )
            }
        }

        assertEquals(
            "/voice_actors?updated_after=1970-01-01T00%3A00%3A00Z&ids=1%2C2",
            mockWebServer.takeRequest().path
        )
    }

    @Test
    fun testFlow() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "object": "collection",
                      "url": "https://api.wanikani.com/v2/voice_actors",
                      "pages": {
                        "per_page": 1,
                        "next_url": "https://api.wanikani.com/v2/voice_actors?page_after_id=234",
                        "previous_url": null
                      },
                      "total_count": 2,
                      "data_updated_at": "2017-11-29T19:37:03.571377Z",
                      "data": [
                        {
                          "id": 234,
                          "object": "voice_actor",
                          "url": "https://api.wanikani.com/v2/voice_actors/1",
                          "data_updated_at": "2017-12-20T00:24:47.048380Z",
                          "data": {
                            "created_at": "2017-12-20T00:03:56.642838Z",
                            "name": "Kyoko",
                            "gender": "female",
                            "description": "Tokyo accent"
                          }
                        }
                      ]
                    }
                """.trimIndent()
                )
            }
        )
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "object": "collection",
                      "url": "https://api.wanikani.com/v2/voice_actors",
                      "pages": {
                        "per_page": 1,
                        "next_url": null,
                        "previous_url": null
                      },
                      "total_count": 2,
                      "data_updated_at": "2017-11-29T19:37:03.571377Z",
                      "data": [
                        {
                          "id": 666,
                          "object": "voice_actor",
                          "url": "https://api.wanikani.com/v2/voice_actors/2",
                          "data_updated_at": "2017-12-20T00:24:47.048380Z",
                          "data": {
                            "created_at": "2017-12-20T00:03:56.642838Z",
                            "name": "Bob",
                            "gender": "male",
                            "description": "Tokyo accent"
                          }
                        }
                      ]
                    }
                """.trimIndent()
                )
            }
        )

        val responses = runBlocking {
            wanikani.getVoiceActorsFlow().toList()
        }

        assertEquals(234L, responses.first().id)
        assertEquals(666L, responses.last().id)

        mockWebServer.takeRequest().let {
            assertNull(
                it.requestUrl?.queryParameter("page_after_id"),
                "Incorrect pageAfterId in ${it.requestUrl}"
            )
        }
        mockWebServer.takeRequest().let {
            assertEquals(
                "234",
                it.requestUrl?.queryParameter("page_after_id"),
                "Incorrect pageAfterId in ${it.requestUrl}"
            )
        }
    }

    @Test
    fun testGetAll() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "object": "collection",
                      "url": "https://api.wanikani.com/v2/voice_actors",
                      "pages": {
                        "per_page": 500,
                        "next_url": null,
                        "previous_url": null
                      },
                      "total_count": 2,
                      "data_updated_at": "2017-11-29T19:37:03.571377Z",
                      "data": [
                        {
                          "id": 234,
                          "object": "voice_actor",
                          "url": "https://api.wanikani.com/v2/voice_actors/1",
                          "data_updated_at": "2017-12-20T00:24:47.048380Z",
                          "data": {
                            "created_at": "2017-12-20T00:03:56.642838Z",
                            "name": "Kyoko",
                            "gender": "female",
                            "description": "Tokyo accent"
                          }
                        }
                      ]
                    }
                """.trimIndent()
                )
            }
        )

        val responses = runBlocking {
            wanikani.getVoiceActors()
        }

        val request = mockWebServer.takeRequest()
        assertEquals("/voice_actors", request.path)

        assertEquals(ObjectType.COLLECTION, responses.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/voice_actors"), responses.url)
        assertEquals(500, responses.pages.perPage)
        assertNull(responses.pages.nextURL)
        assertNull(responses.pages.previousURL)
        assertEquals(2, responses.totalCount)
        assertEquals(Instant.parse("2017-11-29T19:37:03.571377Z"), responses.dataUpdatedAt)

        val response = responses.data.first()

        assertEquals(234, response.id)
        assertEquals(ObjectType.VOICE_ACTOR, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/voice_actors/1"), response.url)
        assertEquals(Instant.parse("2017-12-20T00:24:47.048380Z"), response.dataUpdatedAt)

        val voiceActor = response.data

        assertEquals(Instant.parse("2017-12-20T00:03:56.642838Z"), voiceActor.createdAt)
        assertEquals("Kyoko", voiceActor.name)
        assertEquals("female", voiceActor.gender)
        assertEquals("Tokyo accent", voiceActor.description)
    }

    @Test
    fun testGetSpecific() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "id": 234,
                      "object": "voice_actor",
                      "url": "https://api.wanikani.com/v2/voice_actors/1",
                      "data_updated_at": "2017-12-20T00:24:47.048380Z",
                      "data": {
                        "created_at": "2017-12-20T00:03:56.642838Z",
                        "name": "Kyoko",
                        "gender": "female",
                        "description": "Tokyo accent"
                      }
                    }
                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.getVoiceActor(234)
        }

        assertEquals(234, response.id)
        assertEquals(ObjectType.VOICE_ACTOR, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/voice_actors/1"), response.url)
        assertEquals(Instant.parse("2017-12-20T00:24:47.048380Z"), response.dataUpdatedAt)

        val voiceActor = response.data

        assertEquals(Instant.parse("2017-12-20T00:03:56.642838Z"), voiceActor.createdAt)
        assertEquals("Kyoko", voiceActor.name)
        assertEquals("female", voiceActor.gender)
        assertEquals("Tokyo accent", voiceActor.description)
    }
}
