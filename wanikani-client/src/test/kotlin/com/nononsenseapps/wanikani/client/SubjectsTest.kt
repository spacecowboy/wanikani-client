package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.response.CharacterPngImage
import com.nononsenseapps.wanikani.client.response.CharacterSvgImage
import com.nononsenseapps.wanikani.client.response.ContextSentence
import com.nononsenseapps.wanikani.client.response.ImageContentType
import com.nononsenseapps.wanikani.client.response.KunYomiReading
import com.nononsenseapps.wanikani.client.response.Meaning
import com.nononsenseapps.wanikani.client.response.NanoriReading
import com.nononsenseapps.wanikani.client.response.ObjectType
import com.nononsenseapps.wanikani.client.response.OnYomiReading
import com.nononsenseapps.wanikani.client.response.PngMetadata
import com.nononsenseapps.wanikani.client.response.PronunciationAudio
import com.nononsenseapps.wanikani.client.response.PronunciationAudioMetadata
import com.nononsenseapps.wanikani.client.response.SubjectKanjiResponse
import com.nononsenseapps.wanikani.client.response.SubjectRadicalResponse
import com.nononsenseapps.wanikani.client.response.SubjectType
import com.nononsenseapps.wanikani.client.response.SubjectVocabularyResponse
import com.nononsenseapps.wanikani.client.response.SvgMetadata
import com.nononsenseapps.wanikani.client.response.VocabularyReading
import com.nononsenseapps.wanikani.client.response.WhitelistMeaning
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException
import java.net.URL
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

class SubjectsTest : MockedWebServerTest() {
    @Test
    fun testGetAllQueryParams() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(404)
        )

        runBlocking {
            assertFailsWith<HttpException> {
                wanikani.getSubjects(
                    updatedAfter = Instant.EPOCH,
                    ids = arrayOf(1, 2),
                    types = arrayOf(SubjectType.KANJI, SubjectType.VOCABULARY),
                    slugs = arrayOf("one", "two"),
                    levels = arrayOf(1, 2, 3),
                    hidden = false
                )
            }
        }

        assertEquals(
            "/subjects?ids=1%2C2&types=kanji%2Cvocabulary&slugs=one%2Ctwo&levels=1%2C2%2C3&hidden=false&updated_after=1970-01-01T00%3A00%3A00Z",
            mockWebServer.takeRequest().path
        )
    }

    @Test
    fun testGetAll() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "object": "collection",
                      "url": "https://api.wanikani.com/v2/subjects?types=kanji",
                      "pages": {
                        "per_page": 1000,
                        "next_url": "https://api.wanikani.com/v2/subjects?page_after_id=1439\u0026types=kanji",
                        "previous_url": null
                      },
                      "total_count": 2027,
                      "data_updated_at": "2018-04-09T18:08:59.946969Z",
                      "data": [
                        {
                          "id": 440,
                          "object": "kanji",
                          "url": "https://api.wanikani.com/v2/subjects/440",
                          "data_updated_at": "2018-03-29T23:14:30.805034Z",
                          "data": {
                            "created_at": "2012-02-27T19:55:19.000000Z",
                            "level": 1,
                            "slug": "一",
                            "hidden_at": null,
                            "document_url": "https://www.wanikani.com/kanji/%E4%B8%80",
                            "characters": "一",
                            "meanings": [
                              {
                                "meaning": "One",
                                "primary": true,
                                "accepted_answer": true
                              }
                            ],
                            "readings": [
                              {
                                "type": "onyomi",
                                "primary": true,
                                "accepted_answer": true,
                                "reading": "いち"
                              },
                              {
                                "type": "kunyomi",
                                "primary": false,
                                "accepted_answer": false,
                                "reading": "ひと"
                              },
                              {
                                "type": "nanori",
                                "primary": false,
                                "accepted_answer": false,
                                "reading": "かず"
                              }
                            ],
                            "component_subject_ids": [
                              1
                            ],
                            "amalgamation_subject_ids": [
                              56,
                              88,
                              91
                            ],
                            "visually_similar_subject_ids": [],
                            "meaning_mnemonic": "Lying on the",
                            "meaning_hint": "To remember",
                            "reading_mnemonic": "As you're sitting",
                            "reading_hint": "Make sure you",
                            "lesson_position": 2
                          }
                        }
                      ]
                    }
                """.trimIndent()
                )
            }
        )

        val responses = runBlocking {
            wanikani.getSubjects()
        }

        assertEquals(ObjectType.COLLECTION, responses.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/subjects?types=kanji"), responses.url)
        assertEquals(1000, responses.pages.perPage)
        assertEquals(
            URL("https://api.wanikani.com/v2/subjects?page_after_id=1439&types=kanji"),
            responses.pages.nextURL
        )
        assertNull(responses.pages.previousURL)
        assertEquals(2027, responses.totalCount)
        assertEquals(Instant.parse("2018-04-09T18:08:59.946969Z"), responses.dataUpdatedAt)

        val response = responses.data.first() as SubjectKanjiResponse

        assertEquals(440, response.id)
        assertEquals(ObjectType.KANJI, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/subjects/440"), response.url)
        assertEquals(Instant.parse("2018-03-29T23:14:30.805034Z"), response.dataUpdatedAt)

        val kanji = response.data

        assertEquals(Instant.parse("2012-02-27T19:55:19.000000Z"), kanji.createdAt)
        assertEquals(1, kanji.level)
        assertEquals("一", kanji.slug)
        assertNull(kanji.hiddenAt)
        assertEquals(URL("https://www.wanikani.com/kanji/%E4%B8%80"), kanji.documentUrl)
        assertEquals("一", kanji.characters)
        assertEquals("Lying on the", kanji.meaningMnemonic)
        assertEquals(2, kanji.lessonPosition)
        assertEquals(listOf(56L, 88L, 91L), kanji.amalgamationSubjectIds)
        assertEquals(
            listOf(
                Meaning(
                    meaning = "One",
                    primary = true,
                    acceptedAnswer = true
                )
            ),
            kanji.meanings
        )
        assertNull(kanji.auxiliaryMeanings)
        assertEquals(
            listOf(
                OnYomiReading("いち", primary = true, acceptedAnswer = true),
                KunYomiReading("ひと", primary = false, acceptedAnswer = false),
                NanoriReading("かず", primary = false, acceptedAnswer = false)
            ),
            kanji.readings
        )
        assertEquals("Make sure you", kanji.readingHint)
        assertEquals("As you're sitting", kanji.readingMnemonic)
        assertEquals("To remember", kanji.meaningHint)
        assertEquals(listOf(1L), kanji.componentSubjectIds)
    }

    @Test
    fun testGetSpecificVocabulary() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "id": 2467,
                      "object": "vocabulary",
                      "url": "https://api.wanikani.com/v2/subjects/2467",
                      "data_updated_at": "2020-05-27T16:46:31.058030Z",
                      "data": {
                        "created_at": "2012-02-28T08:04:47.000000Z",
                        "level": 1,
                        "slug": "一",
                        "hidden_at": null,
                        "document_url": "https://www.wanikani.com/vocabulary/%E4%B8%80",
                        "characters": "一",
                        "meanings": [
                          {
                            "meaning": "One",
                            "primary": true,
                            "accepted_answer": true
                          }
                        ],
                        "auxiliary_meanings": [
                          {
                            "type": "whitelist",
                            "meaning": "1"
                          }
                        ],
                        "readings": [
                          {
                            "primary": true,
                            "reading": "いち",
                            "accepted_answer": true
                          }
                        ],
                        "parts_of_speech": [
                          "numeral"
                        ],
                        "component_subject_ids": [
                          440
                        ],
                        "meaning_mnemonic": "As is the case",
                        "reading_mnemonic": "When a vocab",
                        "context_sentences": [
                          {
                            "en": "Let’s meet up once.",
                            "ja": "一ど、あいましょう。"
                          }
                        ],
                        "pronunciation_audios": [
                          {
                            "url": "https://cdn.wanikani.com/audios/27959-subject-2467.ogg?1553788696",
                            "metadata": {
                              "gender": "female",
                              "source_id": 21630,
                              "pronunciation": "いち",
                              "voice_actor_id": 1,
                              "voice_actor_name": "Kyoko",
                              "voice_description": "Tokyo accent"
                            },
                            "content_type": "audio/ogg"
                          },
                          {
                            "url": "https://cdn.wanikani.com/audios/3020-subject-2467.mp3?1547862356",
                            "metadata": {
                              "gender": "male",
                              "source_id": 2711,
                              "pronunciation": "いち",
                              "voice_actor_id": 2,
                              "voice_actor_name": "Kenichi",
                              "voice_description": "Tokyo accent"
                            },
                            "content_type": "audio/webm"
                          }
                        ],
                        "lesson_position": 44,
                        "spaced_repetition_system_id": 2
                      }
                    }

                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.getSubject(440)
        } as SubjectVocabularyResponse

        assertEquals(2467, response.id)
        assertEquals(ObjectType.VOCABULARY, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/subjects/2467"), response.url)
        assertEquals(Instant.parse("2020-05-27T16:46:31.058030Z"), response.dataUpdatedAt)

        val vocabulary = response.data

        assertEquals(Instant.parse("2012-02-28T08:04:47.000000Z"), vocabulary.createdAt)
        assertEquals(1, vocabulary.level)
        assertEquals("一", vocabulary.slug)
        assertNull(vocabulary.hiddenAt)
        assertEquals(URL("https://www.wanikani.com/vocabulary/%E4%B8%80"), vocabulary.documentUrl)
        assertEquals("一", vocabulary.characters)
        assertEquals(
            listOf(
                Meaning(
                    meaning = "One",
                    primary = true,
                    acceptedAnswer = true
                )
            ),
            vocabulary.meanings
        )
        assertEquals(
            listOf(
                WhitelistMeaning(
                    meaning = "1"
                )
            ),
            vocabulary.auxiliaryMeanings
        )
        assertEquals(
            listOf(
                VocabularyReading("いち", primary = true, acceptedAnswer = true)
            ),
            vocabulary.readings
        )
        assertEquals(listOf("numeral"), vocabulary.partsOfSpeech)
        assertEquals(listOf(440L), vocabulary.componentSubjectIds)
        assertEquals("As is the case", vocabulary.meaningMnemonic)
        assertEquals("When a vocab", vocabulary.readingMnemonic)
        assertEquals(
            listOf(
                ContextSentence(
                    en = "Let’s meet up once.",
                    ja = "一ど、あいましょう。"
                )
            ),
            vocabulary.contextSentences
        )
        assertEquals(44, vocabulary.lessonPosition)
        assertEquals(2, vocabulary.spacedRepetitionSystemId)

        assertEquals(
            listOf(
                PronunciationAudio(
                    url = URL("https://cdn.wanikani.com/audios/27959-subject-2467.ogg?1553788696"),
                    contentType = "audio/ogg",
                    metadata = PronunciationAudioMetadata(
                        gender = "female",
                        sourceId = 21630L,
                        pronunciation = "いち",
                        voiceActorId = 1,
                        voiceActorName = "Kyoko",
                        voiceDescription = "Tokyo accent"
                    )
                ),
                PronunciationAudio(
                    url = URL("https://cdn.wanikani.com/audios/3020-subject-2467.mp3?1547862356"),
                    contentType = "audio/webm",
                    metadata = PronunciationAudioMetadata(
                        gender = "male",
                        sourceId = 2711,
                        pronunciation = "いち",
                        voiceActorId = 2,
                        voiceActorName = "Kenichi",
                        voiceDescription = "Tokyo accent"
                    )
                )
            ),
            vocabulary.pronunciationAudios
        )
    }

    @Test
    fun testGetSpecificKanji() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "id": 440,
                      "object": "kanji",
                      "url": "https://api.wanikani.com/v2/subjects/440",
                      "data_updated_at": "2020-05-27T16:43:02.121960Z",
                      "data": {
                        "created_at": "2012-02-27T19:55:19.000000Z",
                        "level": 1,
                        "slug": "一",
                        "hidden_at": null,
                        "document_url": "https://www.wanikani.com/kanji/%E4%B8%80",
                        "characters": "一",
                        "meanings": [
                          {
                            "meaning": "One",
                            "primary": true,
                            "accepted_answer": true
                          }
                        ],
                        "auxiliary_meanings": [
                          {
                            "type": "whitelist",
                            "meaning": "1"
                          }
                        ],
                        "readings": [
                          {
                            "type": "onyomi",
                            "primary": true,
                            "reading": "いち",
                            "accepted_answer": true
                          },
                          {
                            "type": "kunyomi",
                            "primary": false,
                            "reading": "ひと",
                            "accepted_answer": false
                          },
                          {
                            "type": "nanori",
                            "primary": false,
                            "reading": "かず",
                            "accepted_answer": false
                          }
                        ],
                        "component_subject_ids": [
                          1
                        ],
                        "amalgamation_subject_ids": [
                          2467,
                          2468
                        ],
                        "visually_similar_subject_ids": [],
                        "meaning_mnemonic": "Lying on the",
                        "meaning_hint": "To remember",
                        "reading_mnemonic": "As you're sitting",
                        "reading_hint": "Make sure you",
                        "lesson_position": 26,
                        "spaced_repetition_system_id": 2
                      }
                    }
                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.getSubject(440)
        } as SubjectKanjiResponse

        assertEquals(440, response.id)
        assertEquals(ObjectType.KANJI, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/subjects/440"), response.url)
        assertEquals(Instant.parse("2020-05-27T16:43:02.121960Z"), response.dataUpdatedAt)

        val kanji = response.data

        assertEquals(Instant.parse("2012-02-27T19:55:19.000000Z"), kanji.createdAt)
        assertEquals(1, kanji.level)
        assertEquals("一", kanji.slug)
        assertNull(kanji.hiddenAt)
        assertEquals(URL("https://www.wanikani.com/kanji/%E4%B8%80"), kanji.documentUrl)
        assertEquals("一", kanji.characters)
        assertEquals("Lying on the", kanji.meaningMnemonic)
        assertEquals(26, kanji.lessonPosition)
        assertEquals(2, kanji.spacedRepetitionSystemId)
        assertEquals(listOf(2467L, 2468L), kanji.amalgamationSubjectIds)
        assertEquals(
            listOf(
                Meaning(
                    meaning = "One",
                    primary = true,
                    acceptedAnswer = true
                )
            ),
            kanji.meanings
        )
        assertEquals(
            listOf(
                WhitelistMeaning(
                    meaning = "1"
                )
            ),
            kanji.auxiliaryMeanings
        )
        assertEquals(
            listOf(
                OnYomiReading("いち", primary = true, acceptedAnswer = true),
                KunYomiReading("ひと", primary = false, acceptedAnswer = false),
                NanoriReading("かず", primary = false, acceptedAnswer = false)
            ),
            kanji.readings
        )
        assertEquals("Make sure you", kanji.readingHint)
        assertEquals("As you're sitting", kanji.readingMnemonic)
        assertEquals("To remember", kanji.meaningHint)
        assertEquals(listOf(1L), kanji.componentSubjectIds)
    }

    @Test
    fun testGetSpecificRadical() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "id": 1,
                      "object": "radical",
                      "url": "https://api.wanikani.com/v2/subjects/1",
                      "data_updated_at": "2020-06-03T17:55:20.425054Z",
                      "data": {
                        "created_at": "2012-02-27T18:08:16.000000Z",
                        "level": 1,
                        "slug": "ground",
                        "hidden_at": null,
                        "document_url": "https://www.wanikani.com/radicals/ground",
                        "characters": "一",
                        "character_images": [
                          {
                            "url": "https://cdn.wanikani.com/images/98-subject-1-with-css-original.svg?1520987072",
                            "metadata": {
                              "inline_styles": true
                            },
                            "content_type": "image/svg+xml"
                          },
                          {
                            "url": "https://cdn.wanikani.com/images/1054-subject-1-normal-weight-black-512px.png?1520987606",
                            "metadata": {
                              "color": "#000000",
                              "dimensions": "512x512",
                              "style_name": "512px"
                            },
                            "content_type": "image/png"
                          }
                        ],
                        "meanings": [
                          {
                            "meaning": "Ground",
                            "primary": true,
                            "accepted_answer": true
                          }
                        ],
                        "auxiliary_meanings": [],
                        "amalgamation_subject_ids": [
                          440,
                          449
                        ],
                        "meaning_mnemonic": "This radical consists of a single",
                        "lesson_position": 0,
                        "spaced_repetition_system_id": 2
                      }
                    }
                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.getSubject(1)
        } as SubjectRadicalResponse

        assertEquals(1, response.id)
        assertEquals(ObjectType.RADICAL, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/subjects/1"), response.url)
        assertEquals(Instant.parse("2020-06-03T17:55:20.425054Z"), response.dataUpdatedAt)

        val radical = response.data

        assertEquals(Instant.parse("2012-02-27T18:08:16.000000Z"), radical.createdAt)
        assertEquals(1, radical.level)
        assertEquals("ground", radical.slug)
        assertNull(radical.hiddenAt)
        assertEquals(URL("https://www.wanikani.com/radicals/ground"), radical.documentUrl)
        assertEquals("一", radical.characters)
        assertEquals("This radical consists of a single", radical.meaningMnemonic)
        assertEquals(0, radical.lessonPosition)
        assertEquals(2, radical.spacedRepetitionSystemId)
        assertEquals(emptyList(), radical.auxiliaryMeanings)
        assertEquals(listOf(440L, 449L), radical.amalgamationSubjectIds)
        assertEquals(
            listOf(
                Meaning(
                    meaning = "Ground",
                    primary = true,
                    acceptedAnswer = true
                )
            ),
            radical.meanings
        )
        assertEquals(
            listOf(
                CharacterSvgImage(
                    url = URL("https://cdn.wanikani.com/images/98-subject-1-with-css-original.svg?1520987072"),
                    contentType = ImageContentType.SVG,
                    metadata = SvgMetadata(inlineStyles = true)
                ),
                CharacterPngImage(
                    url = URL("https://cdn.wanikani.com/images/1054-subject-1-normal-weight-black-512px.png?1520987606"),
                    contentType = ImageContentType.PNG,
                    metadata = PngMetadata(
                        color = "#000000",
                        dimensions = "512x512",
                        styleName = "512px"
                    )
                )
            )
            ,
            radical.characterImages
        )
    }
}
