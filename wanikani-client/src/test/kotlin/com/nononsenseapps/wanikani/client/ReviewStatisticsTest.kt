package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.response.ObjectType
import com.nononsenseapps.wanikani.client.response.SubjectType
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException
import java.net.URL
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertNull

class ReviewStatisticsTest : MockedWebServerTest() {
    @Test
    fun testGetAllQueryParams() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(404)
        )

        runBlocking {
            assertFailsWith<HttpException> {
                wanikani.getReviewStatistics(
                    updatedAfter = Instant.EPOCH,
                    ids = arrayOf(1, 2),
                    hidden = true,
                    percentagesGreaterThan = 90,
                    percentagesLessThan = 100,
                    subjectIds = arrayOf(3, 4),
                    subjectTypes = arrayOf(SubjectType.VOCABULARY, SubjectType.KANJI)
                )
            }
        }

        assertEquals(
            "/review_statistics?hidden=true&ids=1%2C2&percentages_greater_than=90&percentages_less_than=100&subjects_ids=3%2C4&subject_types=vocabulary%2Ckanji&updated_after=1970-01-01T00%3A00%3A00Z",
            mockWebServer.takeRequest().path
        )
    }

    @Test
    fun testGetAll() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "object": "collection",
                      "url": "https://api.wanikani.com/v2/review_statistics",
                      "pages": {
                        "per_page": 500,
                        "next_url": "https://api.wanikani.com/v2/review_statistics?page_after_id=80461982",
                        "previous_url": null
                      },
                      "total_count": 980,
                      "data_updated_at": "2018-04-06T14:43:14.337681Z",
                      "data": [
                        {
                          "id": 80461982,
                          "object": "review_statistic",
                          "url": "https://api.wanikani.com/v2/review_statistics/80461982",
                          "data_updated_at": "2018-04-03T11:50:31.558505Z",
                          "data": {
                            "created_at": "2017-09-05T23:38:10.964821Z",
                            "subject_id": 8761,
                            "subject_type": "radical",
                            "meaning_correct": 8,
                            "meaning_incorrect": 0,
                            "meaning_max_streak": 8,
                            "meaning_current_streak": 8,
                            "reading_correct": 0,
                            "reading_incorrect": 0,
                            "reading_max_streak": 0,
                            "reading_current_streak": 0,
                            "percentage_correct": 100,
                            "hidden": false
                          }
                        }
                      ]
                    }
                """.trimIndent()
                )
            }
        )

        val responses = runBlocking {
            wanikani.getReviewStatistics()
        }

        assertEquals(ObjectType.COLLECTION, responses.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/review_statistics"), responses.url)
        assertEquals(500, responses.pages.perPage)
        assertEquals(
            URL("https://api.wanikani.com/v2/review_statistics?page_after_id=80461982"),
            responses.pages.nextURL
        )
        assertNull(responses.pages.previousURL)
        assertEquals(980, responses.totalCount)
        assertEquals(Instant.parse("2018-04-06T14:43:14.337681Z"), responses.dataUpdatedAt)

        val reviewStatisticResponse = responses.data.first()

        assertEquals(80461982, reviewStatisticResponse.id)
        assertEquals(ObjectType.REVIEW_STATISTIC, reviewStatisticResponse.`object`)
        assertEquals(
            URL("https://api.wanikani.com/v2/review_statistics/80461982"),
            reviewStatisticResponse.url
        )
        assertEquals(
            Instant.parse("2018-04-03T11:50:31.558505Z"),
            reviewStatisticResponse.dataUpdatedAt
        )

        val reviewStatistic = reviewStatisticResponse.data

        assertEquals(Instant.parse("2017-09-05T23:38:10.964821Z"), reviewStatistic.createdAt)
        assertEquals(8761, reviewStatistic.subjectId)
        assertEquals(SubjectType.RADICAL, reviewStatistic.subjectType)
        assertEquals(8, reviewStatistic.meaningCorrect)
        assertEquals(0, reviewStatistic.meaningIncorrect)
        assertEquals(8, reviewStatistic.meaningMaxStreak)
        assertEquals(8, reviewStatistic.meaningCurrentStreak)
        assertEquals(0, reviewStatistic.readingCorrect)
        assertEquals(0, reviewStatistic.readingIncorrect)
        assertEquals(0, reviewStatistic.readingMaxStreak)
        assertEquals(0, reviewStatistic.readingCurrentStreak)
        assertEquals(100, reviewStatistic.percentageCorrect)
        assertFalse(reviewStatistic.hidden)
    }

    @Test
    fun testGetSpecific() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "id": 80461982,
                      "object": "review_statistic",
                      "url": "https://api.wanikani.com/v2/review_statistics/80461982",
                      "data_updated_at": "2018-04-03T11:50:31.558505Z",
                      "data": {
                        "created_at": "2017-09-05T23:38:10.964821Z",
                        "subject_id": 8761,
                        "subject_type": "radical",
                        "meaning_correct": 8,
                        "meaning_incorrect": 0,
                        "meaning_max_streak": 8,
                        "meaning_current_streak": 8,
                        "reading_correct": 0,
                        "reading_incorrect": 0,
                        "reading_max_streak": 0,
                        "reading_current_streak": 0,
                        "percentage_correct": 100,
                        "hidden": false
                      }
                    }
                """.trimIndent()
                )
            }
        )

        val reviewStatisticResponse = runBlocking {
            wanikani.getReviewStatistic(80461982)
        }

        assertEquals(80461982, reviewStatisticResponse.id)
        assertEquals(ObjectType.REVIEW_STATISTIC, reviewStatisticResponse.`object`)
        assertEquals(
            URL("https://api.wanikani.com/v2/review_statistics/80461982"),
            reviewStatisticResponse.url
        )
        assertEquals(
            Instant.parse("2018-04-03T11:50:31.558505Z"),
            reviewStatisticResponse.dataUpdatedAt
        )

        val reviewStatistic = reviewStatisticResponse.data

        assertEquals(Instant.parse("2017-09-05T23:38:10.964821Z"), reviewStatistic.createdAt)
        assertEquals(8761, reviewStatistic.subjectId)
        assertEquals(SubjectType.RADICAL, reviewStatistic.subjectType)
        assertEquals(8, reviewStatistic.meaningCorrect)
        assertEquals(0, reviewStatistic.meaningIncorrect)
        assertEquals(8, reviewStatistic.meaningMaxStreak)
        assertEquals(8, reviewStatistic.meaningCurrentStreak)
        assertEquals(0, reviewStatistic.readingCorrect)
        assertEquals(0, reviewStatistic.readingIncorrect)
        assertEquals(0, reviewStatistic.readingMaxStreak)
        assertEquals(0, reviewStatistic.readingCurrentStreak)
        assertEquals(100, reviewStatistic.percentageCorrect)
        assertFalse(reviewStatistic.hidden)
    }
}
