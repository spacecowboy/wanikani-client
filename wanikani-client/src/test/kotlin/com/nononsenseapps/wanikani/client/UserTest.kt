package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.request.UpdateUserParams
import com.nononsenseapps.wanikani.client.request.UpdateUserPreferences
import com.nononsenseapps.wanikani.client.request.UpdateUserRequest
import com.nononsenseapps.wanikani.client.response.LessonsPresentationOrder
import com.nononsenseapps.wanikani.client.response.ObjectType
import com.nononsenseapps.wanikani.client.response.SubscriptionType
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import java.net.URL
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNull
import kotlin.test.assertTrue

class UserTest : MockedWebServerTest() {
    @Test
    fun testGet() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "object": "user",
                      "url": "https://api.wanikani.com/v2/user",
                      "data_updated_at": "2018-04-06T14:26:53.022245Z",
                      "data": {
                        "id": "5a6a5234-a392-4a87-8f3f-33342afe8a42",
                        "username": "example_user",
                        "level": 5,
                        "profile_url": "https://www.wanikani.com/users/example_user",
                        "started_at": "2012-05-11T00:52:18.958466Z",
                        "current_vacation_started_at": null,
                        "subscription": {
                          "active": true,
                          "type": "recurring",
                          "max_level_granted": 60,
                          "period_ends_at": "2018-12-11T13:32:19.485748Z"
                        },
                        "preferences": {
                          "default_voice_actor_id": 1,
                          "lessons_autoplay_audio": false,
                          "lessons_batch_size": 5,
                          "lessons_presentation_order": "ascending_level_then_subject",
                          "reviews_autoplay_audio": false,
                          "reviews_display_srs_indicator": true
                        }
                      }
                    }
                """.trimIndent())
            }
        )

        val response = runBlocking {
            wanikani.getUser()
        }

        assertEquals(ObjectType.USER, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/user"), response.url)
        assertEquals(Instant.parse("2018-04-06T14:26:53.022245Z"), response.dataUpdatedAt)

        val user = response.data

        assertEquals("5a6a5234-a392-4a87-8f3f-33342afe8a42", user.id)
        assertEquals(Instant.parse("2012-05-11T00:52:18.958466Z"), user.startedAt)
        assertEquals(5, user.level)
        assertEquals(URL("https://www.wanikani.com/users/example_user"), user.profileUrl)
        assertNull(user.currentVacationStartedAt)

        assertTrue(user.subscription.active)
        assertEquals(SubscriptionType.RECURRING, user.subscription.type)
        assertEquals(60, user.subscription.maxLevelGranted)
        assertEquals(Instant.parse("2018-12-11T13:32:19.485748Z"), user.subscription.periodEndsAt)

        assertEquals(1, user.preferences.defaultVoiceActorId)
        assertFalse(user.preferences.lessonsAutoplayAudio)
        assertEquals(5, user.preferences.lessonsBatchSize)
        assertEquals(LessonsPresentationOrder.ASCENDING_LEVEL_THEN_SUBJECT, user.preferences.lessonsPresentationOrder)
        assertFalse(user.preferences.reviewsAutoplayAudio)
        assertTrue(user.preferences.reviewsDisplaySrsIndicator)
    }

    @Test
    fun testUpdate() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "object": "user",
                      "url": "https://api.wanikani.com/v2/user",
                      "data_updated_at": "2018-04-06T14:26:53.022245Z",
                      "data": {
                        "id": "5a6a5234-a392-4a87-8f3f-33342afe8a42",
                        "username": "example_user",
                        "level": 5,
                        "profile_url": "https://www.wanikani.com/users/example_user",
                        "started_at": "2012-05-11T00:52:18.958466Z",
                        "current_vacation_started_at": null,
                        "subscription": {
                          "active": true,
                          "type": "recurring",
                          "max_level_granted": 60,
                          "period_ends_at": "2018-12-11T13:32:19.485748Z"
                        },
                        "preferences": {
                          "default_voice_actor_id": 1,
                          "lessons_autoplay_audio": false,
                          "lessons_batch_size": 5,
                          "lessons_presentation_order": "ascending_level_then_subject",
                          "reviews_autoplay_audio": false,
                          "reviews_display_srs_indicator": true
                        }
                      }
                    }
                """.trimIndent())
            }
        )

        val response = runBlocking {
            wanikani.updateUser(
                UpdateUserRequest(
                    user = UpdateUserParams(
                        preferences = UpdateUserPreferences(
                            lessonsBatchSize = 6,
                            reviewsDisplaySrsIndicator = false
                        )
                    )
                )
            )
        }

        val request = mockWebServer.takeRequest()

        assertEquals("application/json; charset=UTF-8", request.getHeader("Content-Type"))
        assertEquals("/user", request.path)
        assertEquals(
            """{"user":{"preferences":{"lessons_batch_size":6,"reviews_display_srs_indicator":false}}}""",
            request.body.readUtf8()
        )

        assertEquals(ObjectType.USER, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/user"), response.url)
        assertEquals(Instant.parse("2018-04-06T14:26:53.022245Z"), response.dataUpdatedAt)

        val user = response.data

        assertEquals("5a6a5234-a392-4a87-8f3f-33342afe8a42", user.id)
    }
}
