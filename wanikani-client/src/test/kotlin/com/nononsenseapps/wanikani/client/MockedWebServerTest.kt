package com.nononsenseapps.wanikani.client

import okhttp3.mockwebserver.MockWebServer
import org.junit.After
import org.junit.Before

abstract class MockedWebServerTest {
    protected val mockWebServer = MockWebServer()
    protected lateinit var wanikani: WaniKaniV2

    @Before
    fun setup() {
        mockWebServer.start()
        wanikani = getWaniKaniClient(apiToken = "abc", baseUrl = mockWebServer.url("/"))
    }

    @After
    fun teardown() {
        mockWebServer.shutdown()
    }
}
