package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.request.CreateStudyMaterialRequest
import com.nononsenseapps.wanikani.client.request.StudyMaterialCreateParams
import com.nononsenseapps.wanikani.client.request.StudyMaterialUpdateParams
import com.nononsenseapps.wanikani.client.request.UpdateStudyMaterialRequest
import com.nononsenseapps.wanikani.client.response.ObjectType
import com.nononsenseapps.wanikani.client.response.SubjectType
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException
import java.net.URL
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

class StudyMaterialsTest : MockedWebServerTest() {
    @Test
    fun testGetAllQueryParams() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(404)
        )

        runBlocking {
            assertFailsWith<HttpException> {
                wanikani.getStudyMaterials(
                    updatedAfter = Instant.EPOCH,
                    ids = arrayOf(1, 2),
                    hidden = false,
                    subjectIds = arrayOf(2, 3),
                    subjectTypes = arrayOf(SubjectType.RADICAL, SubjectType.KANJI)
                )
            }
        }

        assertEquals(
            "/study_materials?hidden=false&ids=1%2C2&subject_ids=2%2C3&subject_types=radical%2Ckanji&updated_after=1970-01-01T00%3A00%3A00Z",
            mockWebServer.takeRequest().path
        )
    }

    @Test
    fun testGetAll() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "object": "collection",
                      "url": "https://api.wanikani.com/v2/study_materials",
                      "pages": {
                        "per_page": 500,
                        "next_url": "https://api.wanikani.com/v2/study_materials?page_after_id=52342",
                        "previous_url": null
                      },
                      "total_count": 88,
                      "data_updated_at": "2017-12-21T22:42:11.468155Z",
                      "data": [
                        {
                          "id": 65231,
                          "object": "study_material",
                          "url": "https://api.wanikani.com/v2/study_materials/65231",
                          "data_updated_at": "2017-09-30T01:42:13.453291Z",
                          "data": {
                            "created_at": "2017-09-30T01:42:13.453291Z",
                            "subject_id": 241,
                            "subject_type": "radical",
                            "meaning_note": "I like turtles",
                            "reading_note": "I like durtles",
                            "meaning_synonyms": ["burn", "sizzle"]
                          }
                        }
                      ]
                    }
                """.trimIndent()
                )
            }
        )

        val responses = runBlocking {
            wanikani.getStudyMaterials()
        }

        assertEquals(ObjectType.COLLECTION, responses.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/study_materials"), responses.url)
        assertEquals(500, responses.pages.perPage)
        assertEquals(
            URL("https://api.wanikani.com/v2/study_materials?page_after_id=52342"),
            responses.pages.nextURL
        )
        assertNull(responses.pages.previousURL)
        assertEquals(88, responses.totalCount)
        assertEquals(Instant.parse("2017-12-21T22:42:11.468155Z"), responses.dataUpdatedAt)

        val response = responses.data.first()

        assertEquals(65231, response.id)
        assertEquals(ObjectType.STUDY_MATERIAL, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/study_materials/65231"), response.url)
        assertEquals(Instant.parse("2017-09-30T01:42:13.453291Z"), response.dataUpdatedAt)

        val studyMaterial = response.data

        assertEquals(Instant.parse("2017-09-30T01:42:13.453291Z"), studyMaterial.createdAt)
        assertEquals(241, studyMaterial.subjectId)
        assertEquals(SubjectType.RADICAL, studyMaterial.subjectType)
        assertEquals("I like turtles", studyMaterial.meaningNote)
        assertEquals("I like durtles", studyMaterial.readingNote)
        assertEquals(listOf("burn", "sizzle"), studyMaterial.meaningSynonyms)
    }

    @Test
    fun testGetSpecific() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                     "id": 65231,
                     "object": "study_material",
                     "url": "https://api.wanikani.com/v2/study_materials/65231",
                     "data_updated_at": "2017-09-30T01:42:13.453291Z",
                     "data": {
                       "created_at": "2017-09-30T01:42:13.453291Z",
                       "subject_id": 241,
                       "subject_type": "radical",
                       "meaning_note": "I like turtles",
                       "reading_note": "I like durtles",
                       "meaning_synonyms": ["burn", "sizzle"]
                     }
                    }
                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.getStudyMaterial(65231)
        }

        assertEquals(65231, response.id)
        assertEquals(ObjectType.STUDY_MATERIAL, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/study_materials/65231"), response.url)
        assertEquals(Instant.parse("2017-09-30T01:42:13.453291Z"), response.dataUpdatedAt)

        val studyMaterial = response.data

        assertEquals(Instant.parse("2017-09-30T01:42:13.453291Z"), studyMaterial.createdAt)
        assertEquals(241, studyMaterial.subjectId)
        assertEquals(SubjectType.RADICAL, studyMaterial.subjectType)
        assertEquals("I like turtles", studyMaterial.meaningNote)
        assertEquals("I like durtles", studyMaterial.readingNote)
        assertEquals(listOf("burn", "sizzle"), studyMaterial.meaningSynonyms)
    }

    @Test
    fun testCreate() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                     "id": 234,
                     "object": "study_material",
                     "url": "https://api.wanikani.com/v2/study_materials/234",
                     "data_updated_at": "2017-09-30T01:42:13.453291Z",
                     "data": {
                       "created_at": "2017-09-30T01:42:13.453291Z",
                       "subject_id": 2,
                       "subject_type": "kanji",
                       "meaning_note": "The two grounds is too much",
                       "reading_note": "This is tsu much",
                       "meaning_synonyms": ["double"]
                     }
                    }

                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.createStudyMaterial(
                CreateStudyMaterialRequest(
                    studyMaterial = StudyMaterialCreateParams(
                        subjectId = 2,
                        meaningNote = "The two grounds is too much",
                        readingNote = "This is tsu much",
                        meaningSynonyms = listOf("double")
                    )
                )
            )
        }

        val request = mockWebServer.takeRequest()

        assertEquals("application/json; charset=UTF-8", request.getHeader("Content-Type"))
        assertEquals("/study_materials", request.path)
        assertEquals(
            "{\"study_material\":{\"meaning_note\":\"The two grounds is too much\",\"reading_note\":\"This is tsu much\",\"meaning_synonyms\":[\"double\"],\"subject_id\":2}}",
            request.body.readUtf8()
        )

        assertEquals(234, response.id)
        assertEquals(ObjectType.STUDY_MATERIAL, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/study_materials/234"), response.url)
        assertEquals(Instant.parse("2017-09-30T01:42:13.453291Z"), response.dataUpdatedAt)

        val studyMaterial = response.data

        assertEquals(Instant.parse("2017-09-30T01:42:13.453291Z"), studyMaterial.createdAt)
        assertEquals(2, studyMaterial.subjectId)
        assertEquals(SubjectType.KANJI, studyMaterial.subjectType)
        assertEquals("The two grounds is too much", studyMaterial.meaningNote)
        assertEquals("This is tsu much", studyMaterial.readingNote)
        assertEquals(listOf("double"), studyMaterial.meaningSynonyms)
    }

    @Test
    fun testUpdate() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                     "id": 234,
                     "object": "study_material",
                     "url": "https://api.wanikani.com/v2/study_materials/234",
                     "data_updated_at": "2017-10-15T06:23:43.345321Z",
                     "data": {
                       "created_at": "2017-09-30T01:42:13.453291Z",
                       "subject_id": 2,
                       "subject_type": "kanji",
                       "meaning_note": "The two grounds are on top of each other",
                       "reading_note": "This is too tsu much",
                       "meaning_synonyms": ["double", "twice"]
                     }
                    }
                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.updateStudyMaterial(
                234,
                UpdateStudyMaterialRequest(
                    studyMaterial = StudyMaterialUpdateParams(
                        meaningNote = "The two grounds are on top of each other",
                        readingNote = "This is too tsu much",
                        meaningSynonyms = listOf("double", "twice")
                    )
                )
            )
        }

        val request = mockWebServer.takeRequest()

        assertEquals("application/json; charset=UTF-8", request.getHeader("Content-Type"))
        assertEquals("/study_materials/234", request.path)
        assertEquals(
            "{\"study_material\":{\"meaning_note\":\"The two grounds are on top of each other\",\"reading_note\":\"This is too tsu much\",\"meaning_synonyms\":[\"double\",\"twice\"]}}",
            request.body.readUtf8()
        )

        assertEquals(234, response.id)
        assertEquals(ObjectType.STUDY_MATERIAL, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/study_materials/234"), response.url)
        assertEquals(Instant.parse("2017-10-15T06:23:43.345321Z"), response.dataUpdatedAt)

        val studyMaterial = response.data

        assertEquals(Instant.parse("2017-09-30T01:42:13.453291Z"), studyMaterial.createdAt)
        assertEquals(2, studyMaterial.subjectId)
        assertEquals(SubjectType.KANJI, studyMaterial.subjectType)
        assertEquals("The two grounds are on top of each other", studyMaterial.meaningNote)
        assertEquals("This is too tsu much", studyMaterial.readingNote)
        assertEquals(listOf("double", "twice"), studyMaterial.meaningSynonyms)
    }
}
