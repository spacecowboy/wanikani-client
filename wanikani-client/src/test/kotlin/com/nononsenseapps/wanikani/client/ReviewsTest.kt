package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.request.CreateReviewParams
import com.nononsenseapps.wanikani.client.request.CreateReviewRequest
import com.nononsenseapps.wanikani.client.response.ObjectType
import com.nononsenseapps.wanikani.client.response.SubjectType
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException
import java.net.URL
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertFalse
import kotlin.test.assertNull

class ReviewsTest : MockedWebServerTest() {
    @Test
    fun testGetAllQueryParams() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(404)
        )

        runBlocking {
            assertFailsWith<HttpException> {
                wanikani.getReviews(
                    updatedAfter = Instant.EPOCH,
                    ids = arrayOf(1, 2),
                    subjectIds = arrayOf(2, 3),
                    assignmentIds = arrayOf(3, 4)
                )
            }
        }

        assertEquals(
            "/reviews?updated_after=1970-01-01T00%3A00%3A00Z&ids=1%2C2&subject_ids=2%2C3&assignment_ids=3%2C4",
            mockWebServer.takeRequest().path
        )
    }

    @Test
    fun testGetAllReviews() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "object": "collection",
                      "url": "https://api.wanikani.com/v2/reviews",
                      "pages": {
                        "per_page": 1000,
                        "next_url": "https://api.wanikani.com/v2/reviews?page_after_id=534345",
                        "previous_url": null
                      },
                      "total_count": 19201,
                      "data_updated_at": "2017-12-20T01:10:17.578705Z",
                      "data": [
                        {
                          "id": 534342,
                          "object": "review",
                          "url": "https://api.wanikani.com/v2/reviews/534342",
                          "data_updated_at": "2017-12-20T01:00:59.255427Z",
                          "data": {
                            "created_at": "2017-12-20T01:00:59.255427Z",
                            "assignment_id": 32132,
                            "subject_id": 8,
                            "starting_srs_stage": 4,
                            "starting_srs_stage_name": "Apprentice IV",
                            "spaced_repetition_system_id": 1,
                            "ending_srs_stage": 2,
                            "ending_srs_stage_name": "Apprentice II",
                            "incorrect_meaning_answers": 1,
                            "incorrect_reading_answers": 0
                          }
                        }
                      ]
                    }
                """.trimIndent()
                )
            }
        )

        val responses = runBlocking {
            wanikani.getReviews()
        }

        assertEquals(ObjectType.COLLECTION, responses.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/reviews"), responses.url)
        assertEquals(1000, responses.pages.perPage)
        assertEquals(
            URL("https://api.wanikani.com/v2/reviews?page_after_id=534345"),
            responses.pages.nextURL
        )
        assertNull(responses.pages.previousURL)
        assertEquals(19201, responses.totalCount)
        assertEquals(Instant.parse("2017-12-20T01:10:17.578705Z"), responses.dataUpdatedAt)

        val response = responses.data.first()

        assertEquals(534342, response.id)
        assertEquals(ObjectType.REVIEW, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/reviews/534342"), response.url)
        assertEquals(Instant.parse("2017-12-20T01:00:59.255427Z"), response.dataUpdatedAt)

        val review = response.data

        assertEquals(Instant.parse("2017-12-20T01:00:59.255427Z"), review.createdAt)
        assertEquals(32132, review.assignmentId)
        assertEquals(8, review.subjectId)
        assertEquals(4, review.startingSrsStage)
        assertEquals(1, review.spacedRepetitionSystemId)
        assertEquals(1, review.incorrectMeaningAnswers)
        assertEquals(0, review.incorrectReadingAnswers)
    }

    @Test
    fun testGetReview() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "id": 534342,
                      "object": "review",
                      "url": "https://api.wanikani.com/v2/reviews/80463006",
                      "data_updated_at": "2017-12-20T01:00:59.255427Z",
                      "data": {
                        "created_at": "2017-12-20T01:00:59.255427Z",
                        "assignment_id": 32132,
                        "subject_id": 8,
                        "starting_srs_stage": 4,
                        "starting_srs_stage_name": "Apprentice IV",
                        "spaced_repetition_system_id": 1,
                        "incorrect_meaning_answers": 1,
                        "incorrect_reading_answers": 0
                      }
                    }
                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.getReview(534342)
        }

        assertEquals(534342, response.id)
        assertEquals(ObjectType.REVIEW, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/reviews/80463006"), response.url)
        assertEquals(Instant.parse("2017-12-20T01:00:59.255427Z"), response.dataUpdatedAt)

        val review = response.data

        assertEquals(Instant.parse("2017-12-20T01:00:59.255427Z"), review.createdAt)
        assertEquals(32132, review.assignmentId)
        assertEquals(8, review.subjectId)
        assertEquals(4, review.startingSrsStage)
        assertEquals(1, review.spacedRepetitionSystemId)
        assertEquals(1, review.incorrectMeaningAnswers)
        assertEquals(0, review.incorrectReadingAnswers)
    }

    @Test
    fun testCreateReview() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody(
                    """
                    {
                      "id": 72,
                      "object": "review",
                      "url": "https://api.wanikani.com/v2/reviews/72",
                      "data_updated_at": "2018-05-13T03:34:54.000000Z",
                      "data": {
                        "created_at": "2018-05-13T03:34:54.000000Z",
                        "assignment_id": 1422,
                        "subject_id": 997,
                        "spaced_repetition_system_id": 1,
                        "starting_srs_stage": 1,
                        "starting_srs_stage_name": "Apprentice I",
                        "ending_srs_stage": 1,
                        "ending_srs_stage_name": "Apprentice I",
                        "incorrect_meaning_answers": 1,
                        "incorrect_reading_answers": 2
                      },
                      "resources_updated": {
                        "assignment": {
                          "id": 1422,
                          "object": "assignment",
                          "url": "https://api.wanikani.com/v2/assignments/1422",
                          "data_updated_at": "2018-05-14T03:35:34.180006Z",
                          "data": {
                            "created_at": "2018-01-24T21:32:38.967244Z",
                            "subject_id": 997,
                            "subject_type": "vocabulary",
                            "level": 2,
                            "srs_stage": 1,
                            "srs_stage_name": "Apprentice I",
                            "unlocked_at": "2018-01-24T21:32:39.888359Z",
                            "started_at": "2018-01-24T21:52:47.926376Z",
                            "passed_at": null,
                            "burned_at": null,
                            "available_at": "2018-05-14T07:00:00.000000Z",
                            "resurrected_at": null,
                            "passed": false,
                            "resurrected": false,
                            "hidden": false
                          }
                        },
                        "review_statistic": {
                          "id": 342,
                          "object": "review_statistic",
                          "url": "https://api.wanikani.com/v2/review_statistics/342",
                          "data_updated_at": "2018-05-14T03:35:34.223515Z",
                          "data": {
                            "created_at": "2018-01-24T21:35:55.127513Z",
                            "subject_id": 997,
                            "subject_type": "vocabulary",
                            "meaning_correct": 1,
                            "meaning_incorrect": 1,
                            "meaning_max_streak": 1,
                            "meaning_current_streak": 1,
                            "reading_correct": 1,
                            "reading_incorrect": 2,
                            "reading_max_streak": 1,
                            "reading_current_streak": 1,
                            "percentage_correct": 67,
                            "hidden": false
                          }
                        }
                      }
                    }
                """.trimIndent()
                )
            }
        )

        val response = runBlocking {
            wanikani.createReview(
                CreateReviewRequest(
                    review = CreateReviewParams(
                        assignmentId = 1422,
                        incorrectMeaningAnswers = 1,
                        incorrectReadingAnswers = 2,
                        createdAt = Instant.parse("2017-09-30T01:42:13.453291Z")
                    )
                )
            )
        }

        val request = mockWebServer.takeRequest()

        assertEquals("application/json; charset=UTF-8", request.getHeader("Content-Type"))
        assertEquals("/reviews", request.path)
        assertEquals(
            "{\"review\":{\"assignment_id\":1422,\"incorrect_meaning_answers\":1,\"incorrect_reading_answers\":2,\"created_at\":\"2017-09-30T01:42:13.453291Z\"}}",
            request.body.readUtf8()
        )

        assertEquals(72, response.id)
        assertEquals(ObjectType.REVIEW, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/reviews/72"), response.url)
        assertEquals(Instant.parse("2018-05-13T03:34:54.000000Z"), response.dataUpdatedAt)

        val review = response.data

        assertEquals(Instant.parse("2018-05-13T03:34:54.000000Z"), review.createdAt)
        assertEquals(1422, review.assignmentId)
        assertEquals(997, review.subjectId)
        assertEquals(1, review.startingSrsStage)
        assertEquals(1, review.spacedRepetitionSystemId)
        assertEquals(1, review.incorrectMeaningAnswers)
        assertEquals(2, review.incorrectReadingAnswers)
        assertEquals(1, review.endingSrsStage)

        val updatedResources = response.resourcesUpdated

        val assignmentResponse = updatedResources.assignment
        assertEquals(1422, assignmentResponse.id)
        assertEquals(ObjectType.ASSIGNMENT, assignmentResponse.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/assignments/1422"), assignmentResponse.url)
        assertEquals(Instant.parse("2018-05-14T03:35:34.180006Z"), assignmentResponse.dataUpdatedAt)

        val assignment = assignmentResponse.data

        assertEquals(Instant.parse("2018-01-24T21:32:38.967244Z"), assignment.createdAt)
        assertEquals(997, assignment.subjectId)
        assertEquals(SubjectType.VOCABULARY, assignment.subjectType)
        assertEquals(2, assignment.level)
        assertEquals(1, assignment.srsStage)
        assertEquals(Instant.parse("2018-01-24T21:32:39.888359Z"), assignment.unlockedAt)
        assertEquals(Instant.parse("2018-01-24T21:52:47.926376Z"), assignment.startedAt)
        assertNull(assignment.passedAt)
        assertNull(assignment.burnedAt)
        assertEquals(Instant.parse("2018-05-14T07:00:00.000000Z"), assignment.availableAt)
        assertNull(assignment.resurrectedAt)
        assertFalse(assignment.resurrected)
        assertFalse(assignment.hidden)

        val reviewStatisticResponse = updatedResources.reviewStatistic

        assertEquals(342, reviewStatisticResponse.id)
        assertEquals(ObjectType.REVIEW_STATISTIC, reviewStatisticResponse.`object`)
        assertEquals(
            URL("https://api.wanikani.com/v2/review_statistics/342"),
            reviewStatisticResponse.url
        )
        assertEquals(
            Instant.parse("2018-05-14T03:35:34.223515Z"),
            reviewStatisticResponse.dataUpdatedAt
        )

        val reviewStatistic = reviewStatisticResponse.data

        assertEquals(Instant.parse("2018-01-24T21:35:55.127513Z"), reviewStatistic.createdAt)
        assertEquals(997, reviewStatistic.subjectId)
        assertEquals(SubjectType.VOCABULARY, reviewStatistic.subjectType)
        assertEquals(1, reviewStatistic.meaningCorrect)
        assertEquals(1, reviewStatistic.meaningIncorrect)
        assertEquals(1, reviewStatistic.meaningMaxStreak)
        assertEquals(1, reviewStatistic.meaningCurrentStreak)
        assertEquals(1, reviewStatistic.readingCorrect)
        assertEquals(2, reviewStatistic.readingIncorrect)
        assertEquals(1, reviewStatistic.readingMaxStreak)
        assertEquals(1, reviewStatistic.readingCurrentStreak)
        assertEquals(67, reviewStatistic.percentageCorrect)
        assertFalse(reviewStatistic.hidden)
    }
}
