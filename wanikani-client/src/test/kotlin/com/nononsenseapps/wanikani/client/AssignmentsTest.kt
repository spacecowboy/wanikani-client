package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.request.StartAssignmentRequest
import com.nononsenseapps.wanikani.client.response.ObjectType
import com.nononsenseapps.wanikani.client.response.SubjectType
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import org.junit.Test
import retrofit2.HttpException
import java.net.URL
import java.time.Instant
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNull

class AssignmentsTest: MockedWebServerTest() {
    @Test
    fun testGetAllQueryParams() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(404)
        )

        runBlocking {
            assertFailsWith<HttpException> {
                wanikani.getAssignments(
                    availableAfter = Instant.EPOCH,
                    availableBefore = Instant.EPOCH,
                    burned = false,
                    hidden = true,
                    ids = arrayOf(1,2,3),
                    levels = arrayOf(1,2,3),
                    passed = true,
                    srsStages = arrayOf(2,3),
                    started = false,
                    subjectIds = arrayOf(5,6,7),
                    subjectTypes = arrayOf(SubjectType.KANJI, SubjectType.RADICAL),
                    unlocked = true,
                    updatedAfter = Instant.EPOCH
                )
            }
        }

        assertEquals(
            "/assignments?available_after=1970-01-01T00%3A00%3A00Z&available_before=1970-01-01T00%3A00%3A00Z&burned=false&hidden=true&ids=1%2C2%2C3&levels=1%2C2%2C3&passed=true&srs_stages=2%2C3&started=false&subject_ids=5%2C6%2C7&subject_types=kanji%2Cradical&unlocked=true&updated_after=1970-01-01T00%3A00%3A00Z",
            mockWebServer.takeRequest().path
        )
    }

    @Test
    fun testGetAllAssignments() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "object": "collection",
                      "url": "https://api.wanikani.com/v2/assignments",
                      "pages": {
                        "per_page": 500,
                        "next_url": "https://api.wanikani.com/v2/assignments?page_after_id=80469434",
                        "previous_url": null
                      },
                      "total_count": 1600,
                      "data_updated_at": "2017-11-29T19:37:03.571377Z",
                      "data": [
                        {
                          "id": 80463006,
                          "object": "assignment",
                          "url": "https://api.wanikani.com/v2/assignments/80463006",
                          "data_updated_at": "2017-10-30T01:51:10.438432Z",
                          "data": {
                            "created_at": "2017-09-05T23:38:10.695133Z",
                            "subject_id": 8761,
                            "subject_type": "radical",
                            "srs_stage": 8,
                            "srs_stage_name": "Enlightened",
                            "unlocked_at": "2017-09-05T23:38:10.695133Z",
                            "started_at": "2017-09-05T23:41:28.980679Z",
                            "passed_at": "2017-09-07T17:14:14.491889Z",
                            "burned_at": null,
                            "available_at": "2018-02-27T00:00:00.000000Z",
                            "resurrected_at": null,
                            "passed": true
                          }
                        }
                      ]
                    }
                """.trimIndent())
            }
        )

        val response = runBlocking {
            wanikani.getAssignments()
        }

        assertEquals(ObjectType.COLLECTION, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/assignments"), response.url)
        assertEquals(500, response.pages.perPage)
        assertEquals(URL("https://api.wanikani.com/v2/assignments?page_after_id=80469434"), response.pages.nextURL)
        assertNull(response.pages.previousURL)
        assertEquals(1600, response.totalCount)
        assertEquals(Instant.parse("2017-11-29T19:37:03.571377Z"), response.dataUpdatedAt)
        assertEquals(80469434, response.pages.nextPageAfterId)
        assertNull(response.pages.previousPageAfterId)

        assertEquals(80463006, response.data.first().id)
        assertEquals(ObjectType.ASSIGNMENT, response.data.first().`object`)
        assertEquals(URL("https://api.wanikani.com/v2/assignments/80463006"), response.data.first().url)
        assertEquals(Instant.parse("2017-10-30T01:51:10.438432Z"), response.data.first().dataUpdatedAt)

        val assignment = response.data.first().data

        assertEquals(Instant.parse("2017-09-05T23:38:10.695133Z"), assignment.createdAt)
        assertEquals(8761, assignment.subjectId)
        assertEquals(SubjectType.RADICAL, assignment.subjectType)
        assertEquals(8, assignment.srsStage)
        assertEquals(Instant.parse("2017-09-05T23:38:10.695133Z"), assignment.unlockedAt)
        assertEquals(Instant.parse("2017-09-05T23:41:28.980679Z"), assignment.startedAt)
        assertEquals(Instant.parse("2017-09-07T17:14:14.491889Z"), assignment.passedAt)
        assertNull(assignment.burnedAt)
        assertEquals(Instant.parse("2018-02-27T00:00:00.000000Z"), assignment.availableAt)
        assertNull(assignment.resurrectedAt)
    }

    @Test
    fun testGetAssignment() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "id": 80463006,
                      "object": "assignment",
                      "url": "https://api.wanikani.com/v2/assignments/80463006",
                      "data_updated_at": "2017-11-29T19:37:03.571377Z",
                      "data": {
                        "created_at": "2017-09-05T23:38:10.695133Z",
                        "subject_id": 8761,
                        "subject_type": "radical",
                        "level": 1,
                        "srs_stage": 8,
                        "srs_stage_name": "Enlightened",
                        "unlocked_at": "2017-09-05T23:38:10.695133Z",
                        "started_at": "2017-09-05T23:41:28.980679Z",
                        "passed_at": "2017-09-07T17:14:14.491889Z",
                        "burned_at": null,
                        "available_at": "2018-02-27T00:00:00.000000Z",
                        "passed": true,
                        "resurrected_at": null
                      }
                    }
                """.trimIndent())
            }
        )

        val response = runBlocking {
            wanikani.getAssignment(80463006)
        }

        assertEquals(80463006, response.id)
        assertEquals(ObjectType.ASSIGNMENT, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/assignments/80463006"), response.url)
        assertEquals(Instant.parse("2017-11-29T19:37:03.571377Z"), response.dataUpdatedAt)

        val assignment = response.data

        assertEquals(Instant.parse("2017-09-05T23:38:10.695133Z"), assignment.createdAt)
        assertEquals(8761, assignment.subjectId)
        assertEquals(SubjectType.RADICAL, assignment.subjectType)
        assertEquals(8, assignment.srsStage)
        assertEquals(Instant.parse("2017-09-05T23:38:10.695133Z"), assignment.unlockedAt)
        assertEquals(Instant.parse("2017-09-05T23:41:28.980679Z"), assignment.startedAt)
        assertEquals(Instant.parse("2017-09-07T17:14:14.491889Z"), assignment.passedAt)
        assertNull(assignment.burnedAt)
        assertEquals(Instant.parse("2018-02-27T00:00:00.000000Z"), assignment.availableAt)
        assertNull(assignment.resurrectedAt)
    }

    @Test
    fun testStartAssignmentWithNoDate() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "id": 80463006,
                      "object": "assignment",
                      "url": "https://api.wanikani.com/v2/assignments/80463006",
                      "data_updated_at": "2017-11-29T19:37:03.571377Z",
                      "data": {
                        "created_at": "2017-09-05T23:38:10.695133Z",
                        "subject_id": 8761,
                        "subject_type": "radical",
                        "level": 1,
                        "srs_stage": 8,
                        "srs_stage_name": "Enlightened",
                        "unlocked_at": "2017-09-05T23:38:10.695133Z",
                        "started_at": "2017-09-05T23:41:28.980679Z",
                        "passed_at": "2017-09-07T17:14:14.491889Z",
                        "burned_at": null,
                        "available_at": "2018-02-27T00:00:00.000000Z",
                        "passed": true,
                        "resurrected_at": null
                      }
                    }
                """.trimIndent())
            }
        )

        val response = runBlocking {
            wanikani.startAssignment(80463006, StartAssignmentRequest())
        }

        val request = mockWebServer.takeRequest()

        assertEquals("/assignments/80463006/start", request.path)
        assertEquals("{}", request.body.readUtf8())

        assertEquals(ObjectType.ASSIGNMENT, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/assignments/80463006"), response.url)
        assertEquals(Instant.parse("2017-11-29T19:37:03.571377Z"), response.dataUpdatedAt)

        val assignment = response.data

        assertEquals(Instant.parse("2017-09-05T23:38:10.695133Z"), assignment.createdAt)
        assertEquals(8761, assignment.subjectId)
        assertEquals(SubjectType.RADICAL, assignment.subjectType)
        assertEquals(8, assignment.srsStage)
        assertEquals(Instant.parse("2017-09-05T23:38:10.695133Z"), assignment.unlockedAt)
        assertEquals(Instant.parse("2017-09-05T23:41:28.980679Z"), assignment.startedAt)
        assertEquals(Instant.parse("2017-09-07T17:14:14.491889Z"), assignment.passedAt)
        assertNull(assignment.burnedAt)
        assertEquals(Instant.parse("2018-02-27T00:00:00.000000Z"), assignment.availableAt)
        assertNull(assignment.resurrectedAt)
    }

    @Test
    fun testStartAssignmentWithDate() {
        mockWebServer.enqueue(
            MockResponse().apply {
                setBody("""
                    {
                      "id": 80463006,
                      "object": "assignment",
                      "url": "https://api.wanikani.com/v2/assignments/80463006",
                      "data_updated_at": "2017-11-29T19:37:03.571377Z",
                      "data": {
                        "created_at": "2017-09-05T23:38:10.695133Z",
                        "subject_id": 8761,
                        "subject_type": "radical",
                        "level": 1,
                        "srs_stage": 8,
                        "srs_stage_name": "Enlightened",
                        "unlocked_at": "2017-09-05T23:38:10.695133Z",
                        "started_at": "2017-09-05T23:41:28.980679Z",
                        "passed_at": "2017-09-07T17:14:14.491889Z",
                        "burned_at": null,
                        "available_at": "2018-02-27T00:00:00.000000Z",
                        "passed": true,
                        "resurrected_at": null
                      }
                    }
                """.trimIndent())
            }
        )

        val response = runBlocking {
            wanikani.startAssignment(80463006, StartAssignmentRequest(Instant.parse("2017-09-05T23:41:28.980679Z")))
        }

        val request = mockWebServer.takeRequest()

        assertEquals("/assignments/80463006/start", request.path)
        assertEquals("{\"started_at\":\"2017-09-05T23:41:28.980679Z\"}", request.body.readUtf8())

        assertEquals(ObjectType.ASSIGNMENT, response.`object`)
        assertEquals(URL("https://api.wanikani.com/v2/assignments/80463006"), response.url)
        assertEquals(Instant.parse("2017-11-29T19:37:03.571377Z"), response.dataUpdatedAt)

        val assignment = response.data

        assertEquals(Instant.parse("2017-09-05T23:38:10.695133Z"), assignment.createdAt)
        assertEquals(8761, assignment.subjectId)
        assertEquals(SubjectType.RADICAL, assignment.subjectType)
        assertEquals(8, assignment.srsStage)
        assertEquals(Instant.parse("2017-09-05T23:38:10.695133Z"), assignment.unlockedAt)
        assertEquals(Instant.parse("2017-09-05T23:41:28.980679Z"), assignment.startedAt)
        assertEquals(Instant.parse("2017-09-07T17:14:14.491889Z"), assignment.passedAt)
        assertNull(assignment.burnedAt)
        assertEquals(Instant.parse("2018-02-27T00:00:00.000000Z"), assignment.availableAt)
        assertNull(assignment.resurrectedAt)
    }
}
