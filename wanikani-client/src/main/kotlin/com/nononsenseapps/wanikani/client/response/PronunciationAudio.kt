package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.net.URL

data class PronunciationAudio(
    /**
     * The location of the audio.
     */
    val url: URL,
    /**
     * The content type of the audio. Currently the API delivers audio/mpeg and audio/ogg.
     */
    @Json(name = "content_type")
    val contentType: String,
    /**
     * Details about the pronunciation audio. See table below for details.
     */
    val metadata: PronunciationAudioMetadata
)

