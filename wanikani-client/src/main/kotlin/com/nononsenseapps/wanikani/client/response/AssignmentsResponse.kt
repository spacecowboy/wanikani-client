package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.net.URL
import java.time.Instant

data class AssignmentsResponse(
    override val pages: Pages,
    @Json(name = "total_count")
    override val totalCount: Int,
    override val data: List<AssignmentResponse>,
    override val `object`: ObjectType,
    override val url: URL,
    @Json(name = "data_updated_at")
    override val dataUpdatedAt: Instant?
): WaniKaniCollectionResponse<AssignmentResponse>()
