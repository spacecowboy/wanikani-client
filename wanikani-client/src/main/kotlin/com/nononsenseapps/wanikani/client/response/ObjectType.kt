package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

enum class ObjectType {
    @Json(name = "collection")
    COLLECTION,
    @Json(name = "report")
    REPORT,
    @Json(name = "assignment")
    ASSIGNMENT,
    @Json(name = "kanji")
    KANJI,
    @Json(name = "level_progression")
    LEVEL_PROGRESSION,
    @Json(name = "radical")
    RADICAL,
    @Json(name = "reset")
    RESET,
    @Json(name = "review")
    REVIEW,
    @Json(name = "review_statistic")
    REVIEW_STATISTIC,
    @Json(name = "spaced_repetition_system")
    SPACED_REPETITION_SYSTEM,
    @Json(name = "study_material")
    STUDY_MATERIAL,
    @Json(name = "user")
    USER,
    @Json(name = "vocabulary")
    VOCABULARY,
    @Json(name = "voice_actor")
    VOICE_ACTOR
}
