package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

data class UserSubscription(
    /**
     * Whether or not the user currently has a paid subscription.
     */
    val active: Boolean,
    /**
     * The maximum level of content accessible to the user for lessons, reviews, and content review. For unsubscribed/free users, the maximum level is 3. For subscribed users, this is 60. Any application that uses data from the WaniKani API must respect these access limits.
     */
    @Json(name = "max_level_granted")
    val maxLevelGranted: Int,
    /**
     * The date when the user's subscription period ends. If the user has subscription type lifetime or free then the value is null.
     */
    @Json(name = "period_ends_at")
    val periodEndsAt: Instant?,
    /**
     * The type of subscription the user has. Options are following: free, recurring, and lifetime.
     */
    val type: SubscriptionType
)
