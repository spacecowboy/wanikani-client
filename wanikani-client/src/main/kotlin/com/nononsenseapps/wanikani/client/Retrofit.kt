package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.WaniKaniV2.Companion.API_BASE_URL
import com.nononsenseapps.wanikani.client.WaniKaniV2.Companion.API_REVISION
import com.nononsenseapps.wanikani.client.adapters.InstantAdapter
import com.nononsenseapps.wanikani.client.adapters.StageWithDurationAdapter
import com.nononsenseapps.wanikani.client.adapters.URLAdapter
import com.nononsenseapps.wanikani.client.response.*
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.PolymorphicJsonAdapterFactory
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.create

fun getWaniKaniClient(
    apiToken: String,
    baseUrl: HttpUrl = API_BASE_URL.toHttpUrl(),
    okHttpBuilder: OkHttpClient.Builder = OkHttpClient.Builder()
): WaniKaniV2 {
    val moshi = Moshi.Builder()
        .add(URLAdapter())
        .add(InstantAdapter())
        .add(StageWithDurationAdapter())
        .add(
            PolymorphicJsonAdapterFactory.of(
                SubjectResponse::class.java,
                "object"
            )
                .withSubtype(
                    SubjectRadicalResponse::class.java,
                    SubjectType.RADICAL.name.toLowerCase()
                )
                .withSubtype(
                    SubjectKanjiResponse::class.java,
                    SubjectType.KANJI.name.toLowerCase()
                )
                .withSubtype(
                    SubjectVocabularyResponse::class.java,
                    SubjectType.VOCABULARY.name.toLowerCase()
                )
        )
        .add(
            PolymorphicJsonAdapterFactory.of(
                CharacterImage::class.java,
                "content_type"
            )
                .withSubtype(
                    CharacterSvgImage::class.java,
                    "image/svg+xml"
                )
                .withSubtype(
                    CharacterPngImage::class.java,
                    "image/png"
                )
        )
        .add(
            PolymorphicJsonAdapterFactory.of(
                KanjiReading::class.java,
                "type"
            )
                .withSubtype(
                    OnYomiReading::class.java,
                    ReadingType.ONYOMI.name.toLowerCase()
                )
                .withSubtype(
                    KunYomiReading::class.java,
                    ReadingType.KUNYOMI.name.toLowerCase()
                )
                .withSubtype(
                    NanoriReading::class.java,
                    ReadingType.NANORI.name.toLowerCase()
                )
        )
        .add(
            PolymorphicJsonAdapterFactory.of(
                AuxiliaryMeaning::class.java,
                "type"
            )
                .withSubtype(
                    WhitelistMeaning::class.java,
                    AuxiliaryMeaningType.WHITELIST.name.toLowerCase()
                )
                .withSubtype(
                    BlacklistMeaning::class.java,
                    AuxiliaryMeaningType.BLACKLIST.name.toLowerCase()
                )
        )
        // This should be last
        .add(KotlinJsonAdapterFactory())
        .build()

    val okHttpClient = okHttpBuilder
        .addInterceptor { chain ->
            val request = chain.request().newBuilder()
                .addHeader("Authorization", "Bearer $apiToken")
                .addHeader("Wanikani-Revision", API_REVISION)
                .build()

            chain.proceed(request)
        }
        .build()

    val builder = Retrofit.Builder()
        .client(okHttpClient)
        .baseUrl(baseUrl)
        .addConverterFactory(MoshiConverterFactory.create(moshi))

    val retrofit = builder.build()

    return retrofit.create()
}
