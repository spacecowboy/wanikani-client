package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

/**
 * Users can reset their progress back to any level at or below their current level. When they reset to a particular level, all of the assignments and review_statistics at that level or higher are set back to their default state.
 *
 * Resets contain information about when those resets happen, the starting level, and the target level.
 */
data class Reset(
    /**
     * Timestamp when the user confirmed the reset.
     */
    @Json(name = "confirmed_at")
    val confirmedAt: Instant?,
    /**
     * Timestamp when the reset was created.
     */
    @Json(name = "created_at")
    val createdAt: Instant,
    /**
     * The user's level before the reset, from 1 to 60
     */
    @Json(name = "original_level")
    val originalLevel: Int,
    /**
     * The user's level after the reset, from 1 to 60. It must be less than or equal to original_level.
     */
    @Json(name = "target_level")
    val targetLevel: Int
): WanikaniData

