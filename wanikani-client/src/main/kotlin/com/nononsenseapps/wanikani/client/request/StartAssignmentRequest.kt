package com.nononsenseapps.wanikani.client.request

import com.squareup.moshi.Json
import java.time.Instant

data class StartAssignmentRequest(
    @Json(name = "started_at")
    val startedAt: Instant? = null
)
