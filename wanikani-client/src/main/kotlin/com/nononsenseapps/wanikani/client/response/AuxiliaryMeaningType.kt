package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

enum class AuxiliaryMeaningType {
    @Json(name = "whitelist")
    WHITELIST,
    @Json(name = "blacklist")
    BLACKLIST
}
