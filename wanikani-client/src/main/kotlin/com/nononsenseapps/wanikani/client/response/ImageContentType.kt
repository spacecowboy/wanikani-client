package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

enum class ImageContentType {
    @Json(name = "image/svg+xml")
    SVG,
    @Json(name = "image/png")
    PNG
}
