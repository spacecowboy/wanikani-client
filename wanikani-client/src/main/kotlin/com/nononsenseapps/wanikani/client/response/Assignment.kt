package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

/**
 * Assignments contain information about a user's progress on a particular subject, including their current state and timestamps for various progress milestones. Assignments are created when a user has passed all the components of the given subject and the assignment is at or below their current level for the first time.
 */
data class Assignment(
    /**
     * Timestamp when the related subject will be available in the user's review queue.
     */
    @Json(name = "available_at")
    val availableAt: Instant? = null,
    /**
     * Timestamp when the user reaches SRS stage 9 the first time.
     */
    @Json(name = "burned_at")
    val burnedAt: Instant? = null,
    /**
     * Timestamp when the assignment was created.
     */
    @Json(name = "created_at")
    val createdAt: Instant,
    /**
     * Indicates if the associated subject has been hidden, preventing it from appearing in lessons or reviews.
     */
    val hidden: Boolean = false,
    /**
     * Timestamp when the user reaches SRS stage 5 for the first time.
     */
    @Json(name = "passed_at")
    val passedAt: Instant? = null,
    /**
     * Timestamp when the subject is resurrected and placed back in the user's review queue.
     */
    @Json(name = "resurrected_at")
    val resurrectedAt: Instant? = null,
    /**
     * The current SRS stage interval, from 0 to 9.
     */
    @Json(name = "srs_stage")
    val srsStage: Int,
    /**
     * Timestamp when the user completes the lesson for the related subject.
     */
    @Json(name = "started_at")
    val startedAt: Instant? = null,
    /**
     * Unique identifier of the associated subject.
     */
    @Json(name = "subject_id")
    val subjectId: Long,
    /**
     * The type of the associated subject, one of: kanji, radical, or vocabulary.
     */
    @Json(name = "subject_type")
    val subjectType: SubjectType,
    /**
     * The timestamp when the related subject has its prerequisites satisfied and is made available in lessons.
     *
     * Prerequisites are:
     *
     * - The subject components have reached SRS stage 5 once (they have been “passed”).
     * - The user's level is equal to or greater than the level of the assignment’s subject.
     */
    @Json(name = "unlocked_at")
    val unlockedAt: Instant? = null,

    /**
     * Not mentioned in docs but appears in examples.
     */
    val level: Int? = null,
    /**
     * Not mentioned in docs but appears in examples.
     */
    val resurrected: Boolean = resurrectedAt != null
): WanikaniData
