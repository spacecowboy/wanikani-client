package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

/** Study materials store user-specific notes and synonyms for a given subject. The records are created as soon as the user enters any study information. */
data class StudyMaterial(
    /**
     * Timestamp when the study material was created.
     */
    @Json(name = "created_at")
    val createdAt: Instant,
    /**
     * Indicates if the associated subject has been hidden, preventing it from appearing in lessons or reviews.
     */
    val hidden: Boolean = false,
    /**
     * Free form note related to the meaning(s) of the associated subject.
     */
    @Json(name = "meaning_note")
    val meaningNote: String?,
    /**
     * Synonyms for the meaning of the subject. These are used as additional correct answers during reviews.
     */
    @Json(name = "meaning_synonyms")
    val meaningSynonyms: List<String>,
    /**
     * Free form note related to the reading(s) of the associated subject.
     */
    @Json(name = "reading_note")
    val readingNote: String?,
    /**
     * Unique identifier of the associated subject.
     */
    @Json(name = "subject_id")
    val subjectId: Long,
    /**
     * The type of the associated subject, one of: kanji, radical, or vocabulary.
     */
    @Json(name = "subject_type")
    val subjectType: SubjectType
) : WanikaniData
