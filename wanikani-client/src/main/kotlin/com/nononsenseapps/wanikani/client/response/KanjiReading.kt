package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

sealed class KanjiReading {
    /**
     * A singular subject reading.
     */
    abstract val reading: String
    /**
     * Indicates priority in the WaniKani system.
     */
    abstract val primary: Boolean
    /**
     * Indicates if the reading is used to evaluate user input for correctness.
     */
    abstract val acceptedAnswer: Boolean
    /**
     * The kanji reading's classification: kunyomi, nanori, or onyomi.
     */
    abstract val type: ReadingType
}

data class OnYomiReading(
    override val reading: String,
    override val primary: Boolean,
    @Json(name = "accepted_answer")
    override val acceptedAnswer: Boolean
): KanjiReading() {
    override val type: ReadingType = ReadingType.ONYOMI
}

data class KunYomiReading(
    override val reading: String,
    override val primary: Boolean,
    @Json(name = "accepted_answer")
    override val acceptedAnswer: Boolean
): KanjiReading() {
    override val type: ReadingType = ReadingType.KUNYOMI
}

data class NanoriReading(
    override val reading: String,
    override val primary: Boolean,
    @Json(name = "accepted_answer")
    override val acceptedAnswer: Boolean
): KanjiReading() {
    override val type: ReadingType = ReadingType.NANORI
}
