package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

data class SummaryReview(
    /**
     * When the paired subject_ids are available for reviews. All timestamps are the top of an hour.
     */
    @Json(name = "available_at")
    val availableAt: Instant,
    /**
     * Collection of unique identifiers for subjects.
     */
    @Json(name = "subject_ids")
    val subjectIds: List<Long>
)
