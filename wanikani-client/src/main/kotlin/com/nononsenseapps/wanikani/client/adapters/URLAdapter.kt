package com.nononsenseapps.wanikani.client.adapters

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.net.URL

/**
 * Converts between URL and string
 */
class URLAdapter {
    @ToJson
    fun toJSon(value: URL): String =
        value.toString()

    @FromJson
    fun fromJson(value: String): URL =
        URL(value)
}
