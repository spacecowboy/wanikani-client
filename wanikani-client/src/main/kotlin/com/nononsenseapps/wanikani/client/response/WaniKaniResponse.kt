package com.nononsenseapps.wanikani.client.response

import java.net.URL
import java.time.Instant

abstract class WaniKaniResponse {
    /**
     * The kind of object returned.
     */
    abstract val `object`: ObjectType
    /**
     * The URL of the request. For collections, that will contain all the filters and options you've passed to the API. Resources have a single URL and don't need to be filtered, so the URL will be the same in both resource and collection responses.
     */
    abstract val url: URL
    /**
     * For collections, this is the timestamp of the most recently updated resource in the specified scope and is not limited by pagination. For a resource, then this is the last time that particular resource was updated.
     */
    abstract val dataUpdatedAt: Instant?
}
