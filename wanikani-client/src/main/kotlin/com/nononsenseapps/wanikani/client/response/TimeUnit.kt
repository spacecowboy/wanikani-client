package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.temporal.ChronoUnit
import java.time.temporal.TemporalUnit

enum class TimeUnit(val unit: TemporalUnit) {
    @Json(name ="milliseconds")
    MILLISECONDS(ChronoUnit.MILLIS),
    @Json(name ="seconds")
    SECONDS(ChronoUnit.SECONDS),
    @Json(name ="minutes")
    MINUTES(ChronoUnit.MINUTES),
    @Json(name ="hours")
    HOURS(ChronoUnit.HOURS),
    @Json(name ="days")
    DAYS(ChronoUnit.DAYS),
    @Json(name ="weeks")
    WEEKS(ChronoUnit.WEEKS)
}