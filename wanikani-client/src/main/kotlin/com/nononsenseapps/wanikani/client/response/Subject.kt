package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.net.URL
import java.time.Instant

/**
 * Subjects are the radicals, kanji, and vocabulary that are learned through lessons and reviews. They contain basic dictionary information, such as meanings and/or readings, and information about their relationship to other items with WaniKani, like their level.
 *
 * The exact structure of a subject depends on the subject type. The available subject types are radical, kanji, and vocabulary. Note that any attributes called out for the specific subject type behaves differently than the common attribute of the same name.
 */
sealed class Subject {
    /** Collection of auxiliary meanings. */
    abstract val auxiliaryMeanings: List<AuxiliaryMeaning>?

    /**
     * The UTF-8 characters for the subject, including kanji and hiragana.
     *
     * Unlike kanji and vocabulary, radicals can have a null value for characters. Not all radicals have a UTF entry, so the radical must be visually represented with an image instead.
     * */
    abstract val characters: String?

    /** Timestamp when the subject was created. */
    abstract val createdAt: Instant

    /** A URL pointing to the page on wanikani.com that provides detailed information about this subject. */
    abstract val documentUrl: URL

    /** Timestamp when the subject was hidden, indicating associated assignments will no longer appear in lessons or reviews and that the subject page is no longer visible on wanikani.com. */
    abstract val hiddenAt: Instant?

    /** The position that the subject appears in lessons. Note that the value is scoped to the level of the subject, so there are duplicate values across levels. */
    abstract val lessonPosition: Int

    /** The level of the subject, from 1 to 60. */
    abstract val level: Int

    /** The subject's meaning mnemonic. */
    abstract val meaningMnemonic: String

    /** The subject meanings. */
    abstract val meanings: List<Meaning>

    /** The string that is used when generating the document URL for the subject. Radicals use their meaning, downcased. Kanji and vocabulary use their characters. */
    abstract val slug: String

    /**
     * Unique identifier of the associated spaced_repetition_system.
     */
    abstract val spacedRepetitionSystemId: Long?
}

data class SubjectKanji(
    @Json(name = "auxiliary_meanings")
    override val auxiliaryMeanings: List<AuxiliaryMeaning>? = null,
    override val characters: String?,
    @Json(name = "created_at")
    override val createdAt: Instant,
    @Json(name = "document_url")
    override val documentUrl: URL,
    @Json(name = "hidden_at")
    override val hiddenAt: Instant? = null,
    @Json(name = "lesson_position")
    override val lessonPosition: Int,
    override val level: Int,
    @Json(name = "meaning_mnemonic")
    override val meaningMnemonic: String,
    override val meanings: List<Meaning>,
    override val slug: String,
    @Json(name = "spaced_repetition_system_id")
    override val spacedRepetitionSystemId: Long? = null,
    /**
     * An array of numeric identifiers for the vocabulary that have the kanji as a component.
     */
    @Json(name = "amalgamation_subject_ids")
    val amalgamationSubjectIds: List<Long>,
    /**
     * An array of numeric identifiers for the radicals that make up this kanji. Note that these are the subjects that must have passed assignments in order to unlock this subject's assignment.
     */
    @Json(name = "component_subject_ids")
    val componentSubjectIds: List<Long>,
    /**
     * Selected readings for the kanji. See table below for the object structure.
     */
    val readings: List<KanjiReading>,
    /**
     * An array of numeric identifiers for kanji which are visually similar to the kanji in question.
     */
    @Json(name = "visually_similar_subject_ids")
    val visuallySimilarSubjectIds: List<Long>,
    /**
     * Not mentioned in the docs but it's there
     */
    @Json(name = "reading_hint")
    val readingHint: String? = null,
    /**
     * Not mentioned in the docs but it's there
     */
    @Json(name = "reading_mnemonic")
    val readingMnemonic: String? = null,
    /**
     * Not mentioned in the docs but it's there
     */
    @Json(name = "meaning_hint")
    val meaningHint: String? = null
) : Subject(), WanikaniData

data class SubjectRadical(
    @Json(name = "auxiliary_meanings")
    override val auxiliaryMeanings: List<AuxiliaryMeaning>,
    override val characters: String?,
    @Json(name = "created_at")
    override val createdAt: Instant,
    @Json(name = "document_url")
    override val documentUrl: URL,
    @Json(name = "hidden_at")
    override val hiddenAt: Instant? = null,
    @Json(name = "lesson_position")
    override val lessonPosition: Int,
    override val level: Int,
    @Json(name = "meaning_mnemonic")
    override val meaningMnemonic: String,
    override val meanings: List<Meaning>,
    override val slug: String,
    @Json(name = "spaced_repetition_system_id")
    override val spacedRepetitionSystemId: Long? = null,
    /**
     * An array of numeric identifiers for the kanji that have the radical as a component.
     */
    @Json(name = "amalgamation_subject_ids")
    val amalgamationSubjectIds: List<Long>,
    /**
     * A collection of images of the radical.
     */
    @Json(name = "character_images")
    val characterImages: List<CharacterImage>

) : Subject(), WanikaniData

data class SubjectVocabulary(
    @Json(name = "auxiliary_meanings")
    override val auxiliaryMeanings: List<AuxiliaryMeaning>,
    override val characters: String?,
    @Json(name = "created_at")
    override val createdAt: Instant,
    @Json(name = "document_url")
    override val documentUrl: URL,
    @Json(name = "hidden_at")
    override val hiddenAt: Instant? = null,
    @Json(name = "lesson_position")
    override val lessonPosition: Int,
    override val level: Int,
    @Json(name = "meaning_mnemonic")
    override val meaningMnemonic: String,
    override val meanings: List<Meaning>,
    override val slug: String,
    @Json(name = "spaced_repetition_system_id")
    override val spacedRepetitionSystemId: Long? = null,
    /**
     * An array of numeric identifiers for the kanji that make up this vocabulary. Note that these are the subjects that must be have passed assignments in order to unlock this subject's assignment.
     */
    @Json(name = "component_subject_ids")
    val componentSubjectIds: List<Long>,
    /**
     * A collection of context sentences. See table below for the object structure.
     */
    @Json(name = "context_sentences")
    val contextSentences: List<ContextSentence>,
    /**
     * Parts of speech.
     */
    @Json(name = "parts_of_speech")
    val partsOfSpeech: List<String>,
    /**
     * A collection of pronunciation audio. See table below for the object structure.
     */
    @Json(name = "pronunciation_audios")
    val pronunciationAudios: List<PronunciationAudio>,
    /**
     * Selected readings for the vocabulary. See table below for the object structure.
     */
    val readings: List<VocabularyReading>,
    /**
     * The subject's reading mnemonic.
     */
    @Json(name = "reading_mnemonic")
    val readingMnemonic: String
) : Subject(), WanikaniData
