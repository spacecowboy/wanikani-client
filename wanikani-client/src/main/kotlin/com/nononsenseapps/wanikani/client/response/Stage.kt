package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

data class Stage(
    /**
     * The length of time added to the time of review registration, adjusted to the beginning of the hour.
     */
    val interval: Int?,
    /**
     * Unit of time. Can be the following: milliseconds, seconds, minutes, hours, days, and weeks.
     */
    @Json(name = "interval_unit")
    val intervalUnit: TimeUnit?,
    /**
     * The position of the stage within the continuous order.
     */
    val position: Int
)
