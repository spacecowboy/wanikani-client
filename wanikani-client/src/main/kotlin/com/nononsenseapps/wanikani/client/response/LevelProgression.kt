package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

/**
 * Level progressions contain information about a user's progress through the WaniKani levels.
 *
 * A level progression is created when a user has met the prerequisites for leveling up, which are:
 *
 * - Reach a 90% passing rate on assignments for a user's current level with a subject_type of kanji. Passed assignments have data.passed equal to true and a data.passed_at that's in the past.
 * - Have access to the level. Under /user, the data.level must be less than or equal to data.subscription.max_level_granted.
 */
data class LevelProgression(
    /**
     * Timestamp when the user abandons the level. This is primary used when the user initiates a reset.
     */
    @Json(name = "abandoned_at")
    val abandonedAt: Instant?,
    /**
     * Timestamp when the user burns 100% of the assignments belonging to the associated subject's level.
     */
    @Json(name = "completed_at")
    val completedAt: Instant?,
    /**
     * Timestamp when the level progression is created
     */
    @Json(name = "created_at")
    val createdAt: Instant,
    /**
     * The level of the progression, with possible values from 1 to 60.
     */
    val level: Int,
    /**
     * Timestamp when the user passes at least 90% of the assignments with a type of kanji belonging to the associated subject's level.
     */
    @Json(name = "passed_at")
    val passedAt: Instant?,
    /**
     * Timestamp when the user starts their first lesson of a subject belonging to the level.
     */
    @Json(name = "started_at")
    val startedAt: Instant?,
    /**
     * Timestamp when the user can access lessons and reviews for the level.
     */
    @Json(name = "unlocked_at")
    val unlockedAt: Instant?
): WanikaniData

