package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

data class CreateReviewUpdatedResources(
    val assignment: AssignmentResponse,
    @Json(name = "review_statistic")
    val reviewStatistic: ReviewStatisticResponse
)
