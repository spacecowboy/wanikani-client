package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

data class VocabularyReading(
    /**
     * A singular subject reading.
     */
    val reading: String,
    /**
     * Indicates priority in the WaniKani system.
     */
    val primary: Boolean,
    /**
     * Indicates if the reading is used to evaluate user input for correctness.
     */
    @Json(name = "accepted_answer")
    val acceptedAnswer: Boolean
)
