package com.nononsenseapps.wanikani.client.request

data class UpdateUserRequest(
    val user: UpdateUserParams
)
