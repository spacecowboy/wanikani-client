package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

data class UserPreferences(
    /**
     * The voice actor to be used for lessons and reviews.
     */
    @Json(name = "default_voice_actor_id")
    val defaultVoiceActorId: Long? = null,
    /**
     * Automatically play pronunciation audio for vocabulary during lessons.
     */
    @Json(name = "lessons_autoplay_audio")
    val lessonsAutoplayAudio: Boolean,
    /**
     * Number of subjects introduced to the user during lessons before quizzing.
     */
    @Json(name = "lessons_batch_size")
    val lessonsBatchSize: Int,
    /**
     * The order in which lessons are presented. The options are ascending_level_then_subject, shuffled, and ascending_level_then_shuffled. The default (and best experience) is ascending_level_then_subject.
     */
    @Json(name = "lessons_presentation_order")
    val lessonsPresentationOrder: LessonsPresentationOrder,
    /**
     * Automatically play pronunciation audio for vocabulary during reviews.
     */
    @Json(name = "reviews_autoplay_audio")
    val reviewsAutoplayAudio: Boolean,
    /**
     * Toggle for display SRS change indicator after a subject has been completely answered during review.
     */
    @Json(name = "reviews_display_srs_indicator")
    val reviewsDisplaySrsIndicator: Boolean
)
