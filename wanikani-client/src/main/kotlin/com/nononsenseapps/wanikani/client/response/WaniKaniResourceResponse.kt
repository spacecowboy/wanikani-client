package com.nononsenseapps.wanikani.client.response

abstract class WaniKaniResourceResponse : WaniKaniResponse() {
    /**
     * These are the attributes that are specific to that particular instance and kind of resource.
     */
    abstract val data: WanikaniData
}
