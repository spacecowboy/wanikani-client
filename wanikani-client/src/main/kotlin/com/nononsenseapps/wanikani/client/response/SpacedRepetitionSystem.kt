package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

data class SpacedRepetitionSystem(
    /**
     * Timestamp when the spaced repetition system was created.
     */
    @Json(name = "created_at")
    val createdAt: Instant,
    /**
     * The name of the spaced repetition system
     */
    val name: String,
    /**
     * Details about the spaced repetition system.
     */
    val description: String,

    /**
     * position of the unlocking stage.
     */
    @Json(name = "unlocking_stage_position")
    val unlockingStagePosition: Int,
    /**
     * position of the starting stage.
     */
    @Json(name = "starting_stage_position")
    val startingStagePosition: Int,
    /**
     * position of the passing stage.
     */
    @Json(name = "passing_stage_position")
    val passingStagePosition: Int,
    /**
     * position of the burning stage.
     */
    @Json(name = "burning_stage_position")
    val burningStagePosition: Int,
    /**
     *
     */
    val stages: List<StageWithDuration>
): WanikaniData