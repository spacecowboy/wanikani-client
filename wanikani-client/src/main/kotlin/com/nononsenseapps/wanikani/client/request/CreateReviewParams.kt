package com.nononsenseapps.wanikani.client.request

import com.squareup.moshi.Json
import java.time.Instant

data class CreateReviewParams(
    /**
     * Unique identifier of the assignment. This or subject_id must be set.
     */
    @Json(name = "assignment_id")
    val assignmentId: Long? = null,
    /**
     * Unique identifier of the subject. This or assignment_id must be set.
     */
    @Json(name = "subject_id")
    val subjectId: Long? = null,
    /**
     * Must be zero or a positive number. This is the number of times the meaning was answered incorrectly.
     */
    @Json(name = "incorrect_meaning_answers")
    val incorrectMeaningAnswers: Int,
    /**
     * Must be zero or a positive number. This is the number of times the reading was answered incorrectly. Note that subjects with a type or radical do not quiz on readings. Thus, set this value to 0.
     */
    @Json(name = "incorrect_reading_answers")
    val incorrectReadingAnswers: Int,
    /**
     * Timestamp when the review was completed. Defaults to the time of the request if omitted from the request body. Must be in the past, but after assignment.available_at.
     */
    @Json(name = "created_at")
    val createdAt: Instant? = null
)
