package com.nononsenseapps.wanikani.client.response

abstract class WaniKaniCollectionResponse<T: WaniKaniResourceResponse> : WaniKaniResponse() {
    abstract val pages: Pages

    /**
     * Count of all resources available within the specified scope, not limited to pagination.
     */
    abstract val totalCount: Int

    /**
     * This is going to be the resources returned by the specified scope.
     */
    abstract val data: List<T>
}
