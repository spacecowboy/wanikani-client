package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.net.URL
import java.time.Instant

sealed class SubjectResponse: WaniKaniResourceResponse()

data class SubjectRadicalResponse(
    override val `object`: ObjectType,
    override val url: URL,
    @Json(name = "data_updated_at")
    override val dataUpdatedAt: Instant,
    override val data: SubjectRadical,
    val id: Long
): SubjectResponse()

data class SubjectKanjiResponse(
    override val `object`: ObjectType,
    override val url: URL,
    @Json(name = "data_updated_at")
    override val dataUpdatedAt: Instant,
    override val data: SubjectKanji,
    val id: Long
): SubjectResponse()

data class SubjectVocabularyResponse(
    override val `object`: ObjectType,
    override val url: URL,
    @Json(name = "data_updated_at")
    override val dataUpdatedAt: Instant,
    override val data: SubjectVocabulary,
    val id: Long
): SubjectResponse()
