package com.nononsenseapps.wanikani.client.request

import com.squareup.moshi.Json

data class StudyMaterialCreateParams(
    @Json(name = "meaning_note")
    override val meaningNote: String? = null,
    @Json(name = "reading_note")
    override val readingNote: String? = null,
    @Json(name = "meaning_synonyms")
    override val meaningSynonyms: List<String>? = null,
    @Json(name = "subject_id")
    val subjectId: Long
): StudyMaterialCommonParams()
