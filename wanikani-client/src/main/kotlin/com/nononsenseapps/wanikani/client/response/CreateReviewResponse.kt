package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.net.URL
import java.time.Instant

data class CreateReviewResponse(
    override val `object`: ObjectType,
    override val url: URL,
    @Json(name = "data_updated_at")
    override val dataUpdatedAt: Instant,
    override val data: Review,
    val id: Long,
    @Json(name = "resources_updated")
    val resourcesUpdated: CreateReviewUpdatedResources
): WaniKaniResourceResponse()
