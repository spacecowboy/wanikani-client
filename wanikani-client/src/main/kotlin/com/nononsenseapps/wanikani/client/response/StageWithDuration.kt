package com.nononsenseapps.wanikani.client.response

import java.time.Duration

data class StageWithDuration(
    /**
     * The length of time added to the time of review registration, adjusted to the beginning of the hour.
     */
    val duration: Duration?,
    /**
     * The position of the stage within the continuous order.
     */
    val position: Int
)