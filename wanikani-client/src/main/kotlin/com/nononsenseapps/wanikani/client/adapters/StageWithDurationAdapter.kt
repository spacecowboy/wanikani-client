package com.nononsenseapps.wanikani.client.adapters

import com.nononsenseapps.wanikani.client.response.Stage
import com.nononsenseapps.wanikani.client.response.StageWithDuration
import com.nononsenseapps.wanikani.client.response.TimeUnit
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.Duration
import java.time.temporal.ChronoUnit

/**
 * Converts between StageWithDuration and Stage
 */
class StageWithDurationAdapter {
    @ToJson
    fun toStage(value: StageWithDuration): Stage =
        Stage(
            position = value.position,
            interval = value.duration?.seconds?.toInt(),
            intervalUnit = TimeUnit.SECONDS
        )

    @FromJson
    fun fromStage(value: Stage): StageWithDuration =
        StageWithDuration(
            position = value.position,
            duration = if (value.interval != null && value.intervalUnit != null) {
                Duration.of(
                    value.interval.toLong(),
                    value.intervalUnit.unit
                )
            } else {
                null
            }
        )
}