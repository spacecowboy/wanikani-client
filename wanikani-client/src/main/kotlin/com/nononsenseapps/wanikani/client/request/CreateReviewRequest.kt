package com.nononsenseapps.wanikani.client.request

data class CreateReviewRequest(
    val review: CreateReviewParams
)
