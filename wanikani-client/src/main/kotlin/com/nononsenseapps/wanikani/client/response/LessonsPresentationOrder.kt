package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

enum class LessonsPresentationOrder {
    @Json(name = "ascending_level_then_subject")
    ASCENDING_LEVEL_THEN_SUBJECT,
    @Json(name = "shuffled")
    SHUFFLED,
    @Json(name = "ascending_level_then_shuffled")
    ASCENDING_LEVEL_THEN_SHUFFLED
}
