package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.net.URL

data class Pages(
    /**
     * The URL of the next page of results. If there are no more results, the value is null.
     */
    @Json(name = "next_url")
    val nextURL: URL?,
    /**
     * The URL of the previous page of results. If there are no results at all or no previous page to go to, the value is null.
     */
    @Json(name = "previous_url")
    val previousURL: URL?,
    /**
     * Maximum number of resources delivered for this collection.
     */
    @Json(name = "per_page")
    val perPage: Int
) {
    val nextPageAfterId: Long? = nextURL?.pageAfterId
    val previousPageAfterId: Long? = previousURL?.pageAfterId
}

private val URL.pageAfterId: Long?
    get() = query?.let { query ->
        val result = PAGE_AFTER_ID_REGEX.find(query)?.groupValues?.get(1)
        if (result != null && result.isNotBlank()) {
            result.toLong()
        } else {
            null
        }
    }

private val PAGE_AFTER_ID_REGEX = """page_after_id=(\d+)""".toRegex()