package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

enum class SubjectType {
    @Json(name = "radical")
    RADICAL,
    @Json(name = "kanji")
    KANJI,
    @Json(name = "vocabulary")
    VOCABULARY
}
