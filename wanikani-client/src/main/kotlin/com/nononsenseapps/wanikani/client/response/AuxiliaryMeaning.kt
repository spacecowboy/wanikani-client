package com.nononsenseapps.wanikani.client.response

sealed class AuxiliaryMeaning {
    /**
     * A singular subject meaning.
     */
    abstract val meaning: String

    /**
     * Either whitelist or blacklist. When evaluating user input, whitelisted meanings are used to match for correctness. Blacklisted meanings are used to match for incorrectness.
     */
    abstract val type: AuxiliaryMeaningType
}

data class WhitelistMeaning(override val meaning: String): AuxiliaryMeaning() {
    override val type: AuxiliaryMeaningType = AuxiliaryMeaningType.WHITELIST
}

data class BlacklistMeaning(override val meaning: String): AuxiliaryMeaning() {
    override val type: AuxiliaryMeaningType = AuxiliaryMeaningType.BLACKLIST
}
