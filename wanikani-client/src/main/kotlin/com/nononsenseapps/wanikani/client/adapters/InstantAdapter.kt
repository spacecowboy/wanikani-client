package com.nononsenseapps.wanikani.client.adapters

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.time.Instant

/**
 * Converts between Instant and ISO-formatted date string
 */
class InstantAdapter {
    @ToJson
    fun toJSon(value: Instant): String =
        value.toString()

    @FromJson
    fun fromJson(value: String): Instant =
        Instant.parse(value)
}
