package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.net.URL
import java.time.Instant

/**
 * The user summary returns basic information for the user making the API request, identified by their API key.
 */
data class User(
    /**
     * Id of user
     */
    val id: String,
    /**
     * If the user is on vacation, this will be the timestamp of when that vacation started. If the user is not on vacation, this is null.
     */
    @Json(name = "current_vacation_started_at")
    val currentVacationStartedAt: Instant?,
    /**
     * The current level of the user. This ignores subscription status.
     */
    val level: Int,
    /**
     * User settings specific to the WaniKani application.
     */
    val preferences: UserPreferences,
    /**
     * The URL to the user's public facing profile page.
     */
    @Json(name = "profile_url")
    val profileUrl: URL,
    /**
     * The signup date for the user.
     */
    @Json(name = "started_at")
    val startedAt: Instant,
    /**
     * Details about the user's subscription state.
     */
    val subscription: UserSubscription,
    /**
     * The user's username.
     */
    val username: String
): WanikaniData
