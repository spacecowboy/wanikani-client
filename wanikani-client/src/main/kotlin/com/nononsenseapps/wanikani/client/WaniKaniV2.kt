package com.nononsenseapps.wanikani.client

import com.nononsenseapps.wanikani.client.request.CreateReviewRequest
import com.nononsenseapps.wanikani.client.request.CreateStudyMaterialRequest
import com.nononsenseapps.wanikani.client.request.StartAssignmentRequest
import com.nononsenseapps.wanikani.client.request.UpdateStudyMaterialRequest
import com.nononsenseapps.wanikani.client.request.UpdateUserRequest
import com.nononsenseapps.wanikani.client.response.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path
import retrofit2.http.Query
import java.time.Instant

interface WaniKaniV2 {
    /**
     * Returns a collection of all assignments, ordered by ascending created_at, 500 at a time.
     *
     * @param availableAfter Only assignments available at or after this time are returned.
     * @param availableBefore Only assignments available at or before this time are returned.
     * @param hidden When set to true, returns assignments that have a value in data.burned_at. Returns assignments with a null data.burned_at if false.
     * @param ids Return assignments with a matching value in the hidden attribute
     * @param levels Only assignments where the associated subject level matches one of the array values are returned. Valid values range from 1 to 60.
     * @param passed Returns assignments where data.passed equals passed.
     * @param srsStages Only assignments where data.srs_stage matches one of the array values are returned. Valid values range from 0 to 9
     * @param started When set to true, returns assignments that have a value in data.started_at. Returns assignments with a null data.started_at if false.
     * @param subjectIds Only assignments where data.subject_id matches one of the array values are returned.
     * @param subjectTypes Only assignments where data.subject_type matches one of the array values are returned. Valid values are: radical, kanji, or vocabulary.
     * @param unlocked When set to true, returns assignments that have a value in data.unlocked_at. Returns assignments with a null data.unlocked_at if false.
     * @param updatedAfter Only assignments updated after this time are returned.
     */
    @GET("assignments")
    suspend fun getAssignments(
        @Query("available_after") availableAfter: Instant? = null,
        @Query("available_before") availableBefore: Instant? = null,
        @Query("burned") burned: Boolean? = null,
        @Query("hidden") hidden: Boolean? = null,
        @Query("ids") ids: String? = null,
        @Query("levels") levels: String? = null,
        @Query("passed") passed: Boolean? = null,
        @Query("srs_stages") srsStages: String? = null,
        @Query("started") started: Boolean? = null,
        @Query("subject_ids") subjectIds: String? = null,
        @Query("subject_types") subjectTypes: String? = null,
        @Query("unlocked") unlocked: Boolean? = null,
        @Query("updated_after") updatedAfter: Instant? = null,
        @Query("page_after_id") pageAfterId: Long? = null
    ): AssignmentsResponse

    /**
     * Retrieves a specific assignment by its id.
     *
     * @param id id of assignment
     */
    @GET("assignments/{id}")
    suspend fun getAssignment(@Path("id") id: Long): AssignmentResponse

    /**
     * Mark the assignment as started, moving the assignment from the lessons queue to the review queue. Returns the updated assignment.
     *
     * @param id id of assignment
     * @param startAssignmentRequest parameters
     */
    @PUT("assignments/{id}/start")
    suspend fun startAssignment(
        @Path("id") id: Long,
        @Body startAssignmentRequest: StartAssignmentRequest
    ): AssignmentResponse

    /**
     * Returns a collection of all level progressions, ordered by ascending created_at, 500 at a time.
     *
     * Logging for this endpoint has been implemented late in the application's life. Therefore, some Users will not have a full history.
     *
     * @param ids Only level progressions where data.id matches one of the array values are returned.
     * @param updatedAfter Only level_progressions updated after this time are returned.
     */
    @GET("level_progressions")
    suspend fun getLevelProgressions(
        @Query("updated_after") updatedAfter: Instant? = null,
        @Query("ids") ids: String? = null,
        @Query("page_after_id") pageAfterId: Long? = null
    ): LevelProgressionsResponse

    /**
     * Retrieves a specific level progression by its id.
     *
     * @param id id of level progression
     */
    @GET("level_progressions/{id}")
    suspend fun getLevelProgression(@Path("id") id: Long): LevelProgressionResponse

    /**
     * Returns a collection of all resets, ordered by ascending created_at, 500 at a time.
     *
     * @param ids Only resets updated after this time are returned.
     * @param updatedAfter Only resets where data.id matches one of the array values are returned.
     */
    @GET("resets")
    suspend fun getResets(
        @Query("updated_after") updatedAfter: Instant? = null,
        @Query("ids") ids: String? = null,
        @Query("page_after_id") pageAfterId: Long? = null
    ): ResetsResponse

    /**
     * Retrieves a specific reset by its id.
     *
     * @param id id of reset
     */
    @GET("resets/{id}")
    suspend fun getReset(@Path("id") id: Long): ResetResponse

    /**
     * Returns a collection of all reviews, ordered by ascending created_at, 1000 at a time.
     *
     * @param updatedAfter Only reviews updated after this time are returned.
     * @param ids Only reviews where data.id matches one of the array values are returned.
     * @param subjectIds Only reviews where data.subject_id matches one of the array values are returned.
     * @param assignmentIds Only reviews where data.assignment_id matches one of the array values are returned.
     */
    @GET("reviews")
    suspend fun getReviews(
        @Query("updated_after") updatedAfter: Instant? = null,
        @Query("ids") ids: String? = null,
        @Query("subject_ids") subjectIds: String? = null,
        @Query("assignment_ids") assignmentIds: String? = null,
        @Query("page_after_id") pageAfterId: Long? = null
    ): ReviewsResponse

    /**
     * Retrieves a specific review by its id.
     */
    @GET("reviews/{id}")
    suspend fun getReview(@Path("id") id: Long): ReviewResponse

    /**
     * Creates a review for a specific assignment_id. Using the related subject_id is also a valid alternative to using assignment_id.
     *
     * Some criteria must be met in order for a review to be created: available_at must be not null and in the past.
     *
     * When a review is registered, the associated assignment and review_statistic are both updated. These are returned in the response body under resources_updated.
     *
     * @param createReviewRequest parameters
     */
    @POST("reviews")
    suspend fun createReview(@Body createReviewRequest: CreateReviewRequest): CreateReviewResponse

    /**
     * Returns a collection of all review statistics, ordered by ascending created_at, 500 at a time.
     *
     * @param hidden Return review statistics with a matching value in the hidden attribute
     * @param ids Only review statistics where data.id matches one of the array values are returned.
     * @param percentagesGreaterThan Return review statistics where the percentage_correct is greater than the value.
     * @param percentagesLessThan Return review statistics where the percentage_correct is less than the value.
     * @param subjectIds Only review statistics where data.subject_id matches one of the array values are returned.
     * @param subjectTypes Only review statistics where data.subject_type matches one of the array values are returned. Valid values are: radical, kanji, or vocabulary.
     * @param updatedAfter Only review statistics updated after this time are returned.
     */
    @GET("review_statistics")
    suspend fun getReviewStatistics(
        @Query("hidden") hidden: Boolean? = null,
        @Query("ids") ids: String? = null,
        @Query("percentages_greater_than") percentagesGreaterThan: Int? = null,
        @Query("percentages_less_than") percentagesLessThan: Int? = null,
        @Query("subjects_ids") subjectIds: String? = null,
        @Query("subject_types") subjectTypes: String? = null,
        @Query("updated_after") updatedAfter: Instant? = null,
        @Query("page_after_id") pageAfterId: Long? = null
    ): ReviewStatisticsResponse

    /**
     * Retrieves a specific review statistic by its id.
     *
     * @param id id of review statistic
     */
    @GET("review_statistics/{id}")
    suspend fun getReviewStatistic(@Path("id") id: Long): ReviewStatisticResponse

    /**
     * Returns a collection of all spaced_repetition_systems, ordered by ascending id, 500 at a time.
     */
    @GET("spaced_repetition_systems")
    suspend fun getSpacedRepetitionSystems(
        @Query("ids") ids: String? = null,
        @Query("updated_after") updatedAfter: Instant? = null,
        @Query("page_after_id") pageAfterId: Long? = null
    ): SpacedRepetitionSystemsResponse

    /**
     * Retrieves a specific spaced_repetition_system by its id.
     */
    @GET("spaced_repetition_systems/{id}")
    suspend fun getSpacedRepetitionSystem(
        @Path("id") id: Long
    ): SpacedRepetitionSystemResponse

    /**
     * Returns a collection of all study material, ordered by ascending created_at, 500 at a time.
     *
     * @param hidden Return study materials with a matching value in the hidden attribute
     * @param ids Only study material records where data.id matches one of the array values are returned.
     * @param subjectIds Only study material records where data.subject_id matches one of the array values are returned.
     * @param subjectTypes Only study material records where data.subject_type matches one of the array values are returned. Valid values are: radical, kanji, or vocabulary.
     * @param updatedAfter Only study material records updated after this time are returned.
     */
    @GET("study_materials")
    suspend fun getStudyMaterials(
        @Query("hidden") hidden: Boolean? = null,
        @Query("ids") ids: String? = null,
        @Query("subject_ids") subjectIds: String? = null,
        @Query("subject_types") subjectTypes: String? = null,
        @Query("updated_after") updatedAfter: Instant? = null,
        @Query("page_after_id") pageAfterId: Long? = null
    ): StudyMaterialsResponse

    /**
     * Retrieves a specific study material by its id.
     *
     * @param id id of study material
     */
    @GET("study_materials/{id}")
    suspend fun getStudyMaterial(@Path("id") id: Long): StudyMaterialResponse

    /**
     * Creates a study material for a specific subject_id.
     *
     * The owner of the api key can only create one study_material per subject_id.
     *
     * @param body parameters
     */
    @POST("study_materials")
    suspend fun createStudyMaterial(@Body body: CreateStudyMaterialRequest): StudyMaterialResponse

    /**
     * Updates a study material for a specific id.
     *
     * @param id id of study material
     * @param body parameters
     */
    @PUT("study_materials/{id}")
    suspend fun updateStudyMaterial(
        @Path("id") id: Long,
        @Body body: UpdateStudyMaterialRequest
    ): StudyMaterialResponse

    /**
     * Returns a collection of all subjects, ordered by ascending created_at, 1000 at a time.
     *
     * @param ids Only subjects where data.id matches one of the array values are returned.
     * @param types Return subjects of the specified types.
     * @param slugs Return subjects of the specified slug.
     * @param levels Return subjects at the specified levels.
     * @param hidden Return subjects which are or are not hidden from the user-facing application.
     * @param updatedAfter Only subjects updated after this time are returned.
     */
    @GET("subjects")
    suspend fun getSubjects(
        @Query("ids") ids: String? = null,
        @Query("types") types: String? = null,
        @Query("slugs") slugs: String? = null,
        @Query("levels") levels: String? = null,
        @Query("hidden") hidden: Boolean? = null,
        @Query("updated_after") updatedAfter: Instant? = null,
        @Query("page_after_id") pageAfterId: Long? = null
    ): SubjectsResponse

    /**
     * Retrieves a specific subject by its id. The structure of the response depends on the subject type.
     *
     * @param id id of subject
     *
     * @return a type depending on the request - see [SubjectResponse]
     */
    @GET("subjects/{id}")
    suspend fun getSubject(@Path("id") id: Long): SubjectResponse

    /**
     * Retrieves a summary report.
     */
    @GET("summary")
    suspend fun getSummary(): SummaryResponse

    /**
     * Returns a summary of user information.
     */
    @GET("user")
    suspend fun getUser(): UserResponse

    /**
     * Returns a summary of user information.
     *
     * @param body parameters
     */
    @PUT("user")
    suspend fun updateUser(@Body body: UpdateUserRequest): UserResponse

    /**
     * Returns a collection of all voice_actors, ordered by ascending created_at, 500 at a time.
     *
     * @param updatedAfter Only voice actors updated after this time are returned.
     * @param ids ids of voice actors to return
     */
    @GET("voice_actors")
    suspend fun getVoiceActors(
        @Query("updated_after") updatedAfter: Instant? = null,
        @Query("ids") ids: String? = null,
        @Query("page_after_id") pageAfterId: Long? = null
    ): VoiceActorsResponse

    /**
     * Retrieves a specific voice_actor by its id.
     *
     * @param id id of voice actor
     */
    @GET("voice_actors/{id}")
    suspend fun getVoiceActor(@Path("id") id: Long): VoiceActorResponse

    companion object {
        const val API_REVISION = "20170710"
        const val API_BASE_URL = "https://api.wanikani.com/v2/"
    }
}

/**
 * Returns a collection of all assignments, ordered by ascending created_at, 500 at a time.
 *
 * @param availableAfter Only assignments available at or after this time are returned.
 * @param availableBefore Only assignments available at or before this time are returned.
 * @param hidden When set to true, returns assignments that have a value in data.burned_at. Returns assignments with a null data.burned_at if false.
 * @param ids Return assignments with a matching value in the hidden attribute
 * @param levels Only assignments where the associated subject level matches one of the array values are returned. Valid values range from 1 to 60.
 * @param passed Returns assignments where data.passed equals passed.
 * @param srsStages Only assignments where data.srs_stage matches one of the array values are returned. Valid values range from 0 to 9
 * @param started When set to true, returns assignments that have a value in data.started_at. Returns assignments with a null data.started_at if false.
 * @param subjectIds Only assignments where data.subject_id matches one of the array values are returned.
 * @param subjectTypes Only assignments where data.subject_type matches one of the array values are returned. Valid values are: radical, kanji, or vocabulary.
 * @param unlocked When set to true, returns assignments that have a value in data.unlocked_at. Returns assignments with a null data.unlocked_at if false.
 * @param updatedAfter Only assignments updated after this time are returned.
 */
suspend fun WaniKaniV2.getAssignments(
    availableAfter: Instant? = null,
    availableBefore: Instant? = null,
    burned: Boolean? = null,
    hidden: Boolean? = null,
    ids: Array<Int>? = null,
    levels: Array<Int>? = null,
    passed: Boolean? = null,
    srsStages: Array<Int>? = null,
    started: Boolean? = null,
    subjectIds: Array<Int>? = null,
    subjectTypes: Array<SubjectType>? = null,
    unlocked: Boolean? = null,
    updatedAfter: Instant? = null,
    pageAfterId: Long? = null
) = getAssignments(
    availableAfter = availableAfter,
    availableBefore = availableBefore,
    burned = burned,
    hidden = hidden,
    ids = ids?.joinToString(separator = ","),
    levels = levels?.joinToString(separator = ","),
    passed = passed,
    srsStages = srsStages?.joinToString(separator = ","),
    started = started,
    subjectIds = subjectIds?.joinToString(separator = ","),
    subjectTypes = subjectTypes?.joinToString(separator = ",") { it.name.toLowerCase() },
    unlocked = unlocked,
    updatedAfter = updatedAfter,
    pageAfterId = pageAfterId
)

/**
 * Returns a collection of all level progressions, ordered by ascending created_at, 500 at a time.
 *
 * Logging for this endpoint has been implemented late in the application's life. Therefore, some Users will not have a full history.
 *
 * @param ids Only level progressions where data.id matches one of the array values are returned.
 * @param updatedAfter Only level_progressions updated after this time are returned.
 */
suspend fun WaniKaniV2.getLevelProgressions(
    updatedAfter: Instant? = null,
    ids: Array<Int>? = null,
    pageAfterId: Long? = null
) = getLevelProgressions(
    updatedAfter = updatedAfter,
    ids = ids?.joinToString(separator = ","),
    pageAfterId = pageAfterId
)

/**
 * Returns a collection of all resets, ordered by ascending created_at, 500 at a time.
 *
 * @param ids Only resets updated after this time are returned.
 * @param updatedAfter Only resets where data.id matches one of the array values are returned.
 */
suspend fun WaniKaniV2.getResets(
    updatedAfter: Instant? = null,
    ids: Array<Int>? = null,
    pageAfterId: Long? = null
) = getResets(
    updatedAfter = updatedAfter,
    ids = ids?.joinToString(separator = ","),
    pageAfterId = pageAfterId
)

/**
 * Returns a collection of all reviews, ordered by ascending created_at, 1000 at a time.
 *
 * @param updatedAfter Only reviews updated after this time are returned.
 * @param ids Only reviews where data.id matches one of the array values are returned.
 * @param subjectIds Only reviews where data.subject_id matches one of the array values are returned.
 * @param assignmentIds Only reviews where data.assignment_id matches one of the array values are returned.
 */
suspend fun WaniKaniV2.getReviews(
    updatedAfter: Instant? = null,
    ids: Array<Int>? = null,
    subjectIds: Array<Int>? = null,
    assignmentIds: Array<Int>? = null,
    pageAfterId: Long? = null
) = getReviews(
    updatedAfter = updatedAfter,
    ids = ids?.joinToString(separator = ","),
    subjectIds = subjectIds?.joinToString(separator = ","),
    assignmentIds = assignmentIds?.joinToString(separator = ","),
    pageAfterId = pageAfterId
)

/**
 * Returns a collection of all review statistics, ordered by ascending created_at, 500 at a time.
 *
 * @param hidden Return review statistics with a matching value in the hidden attribute
 * @param ids Only review statistics where data.id matches one of the array values are returned.
 * @param percentagesGreaterThan Return review statistics where the percentage_correct is greater than the value.
 * @param percentagesLessThan Return review statistics where the percentage_correct is less than the value.
 * @param subjectIds Only review statistics where data.subject_id matches one of the array values are returned.
 * @param subjectTypes Only review statistics where data.subject_type matches one of the array values are returned. Valid values are: radical, kanji, or vocabulary.
 * @param updatedAfter Only review statistics updated after this time are returned.
 */
suspend fun WaniKaniV2.getReviewStatistics(
    hidden: Boolean? = null,
    ids: Array<Int>? = null,
    percentagesGreaterThan: Int? = null,
    percentagesLessThan: Int? = null,
    subjectIds: Array<Int>? = null,
    subjectTypes: Array<SubjectType>? = null,
    updatedAfter: Instant? = null,
    pageAfterId: Long? = null
) = getReviewStatistics(
    hidden = hidden,
    ids = ids?.joinToString(separator = ","),
    percentagesGreaterThan = percentagesGreaterThan,
    percentagesLessThan = percentagesLessThan,
    subjectIds = subjectIds?.joinToString(separator = ","),
    updatedAfter = updatedAfter,
    subjectTypes = subjectTypes?.joinToString(separator = ",") { it.name.toLowerCase() },
    pageAfterId = pageAfterId
)

/**
 * Returns a collection of all study material, ordered by ascending created_at, 500 at a time.
 *
 * @param hidden Return study materials with a matching value in the hidden attribute
 * @param ids Only study material records where data.id matches one of the array values are returned.
 * @param subjectIds Only study material records where data.subject_id matches one of the array values are returned.
 * @param subjectTypes Only study material records where data.subject_type matches one of the array values are returned. Valid values are: radical, kanji, or vocabulary.
 * @param updatedAfter Only study material records updated after this time are returned.
 */
suspend fun WaniKaniV2.getStudyMaterials(
    hidden: Boolean? = null,
    ids: Array<Int>? = null,
    subjectIds: Array<Int>? = null,
    subjectTypes: Array<SubjectType>? = null,
    updatedAfter: Instant? = null,
    pageAfterId: Long? = null
) = getStudyMaterials(
    hidden = hidden,
    ids = ids?.joinToString(separator = ","),
    subjectIds = subjectIds?.joinToString(separator = ","),
    subjectTypes = subjectTypes?.joinToString(separator = ",") { it.name.toLowerCase() },
    updatedAfter = updatedAfter,
    pageAfterId = pageAfterId
)

suspend fun WaniKaniV2.getSpacedRepetitionSystems(
    ids: Array<Int>? = null,
    updatedAfter: Instant? = null,
    pageAfterId: Long? = null
) = getSpacedRepetitionSystems(
    ids = ids?.joinToString(separator = ","),
    updatedAfter = updatedAfter,
    pageAfterId = pageAfterId
)

/**
 * Returns a collection of all subjects, ordered by ascending created_at, 1000 at a time.
 *
 * @param ids Only subjects where data.id matches one of the array values are returned.
 * @param types Return subjects of the specified types.
 * @param slugs Return subjects of the specified slug.
 * @param levels Return subjects at the specified levels.
 * @param hidden Return subjects which are or are not hidden from the user-facing application.
 * @param updatedAfter Only subjects updated after this time are returned.
 */
suspend fun WaniKaniV2.getSubjects(
    ids: Array<Int>? = null,
    types: Array<SubjectType>? = null,
    slugs: Array<String>? = null,
    levels: Array<Int>? = null,
    hidden: Boolean? = null,
    updatedAfter: Instant? = null,
    pageAfterId: Long? = null
) = getSubjects(
    ids = ids?.joinToString(separator = ","),
    types = types?.joinToString(separator = ",") { it.name.toLowerCase() },
    slugs = slugs?.joinToString(separator = ","),
    levels = levels?.joinToString(separator = ","),
    hidden = hidden,
    updatedAfter = updatedAfter,
    pageAfterId = pageAfterId
)

/**
 * Returns a collection of all voice_actors, ordered by ascending created_at, 500 at a time.
 *
 * @param updatedAfter Only voice actors updated after this time are returned.
 * @param ids ids of voice actors to return
 */
suspend fun WaniKaniV2.getVoiceActors(
    updatedAfter: Instant? = null,
    ids: Array<Int>? = null,
    pageAfterId: Long? = null
) = getVoiceActors(
    updatedAfter = updatedAfter,
    ids = ids?.joinToString(separator = ","),
    pageAfterId = pageAfterId
)

private fun <T: WaniKaniResourceResponse> flowOfResponses(
    initialPageAfterId: Long? = null,
    block: suspend (Long?) -> WaniKaniCollectionResponse<T>
): Flow<T> = flow {
    var initial = true
    var nextPageAfterId: Long? = initialPageAfterId

    while (initial || nextPageAfterId != null) {
        initial = false

        val collection = block(nextPageAfterId)

        nextPageAfterId = collection.pages.nextPageAfterId

        for (response in collection.data) {
            emit(response)
        }
    }
}

fun WaniKaniV2.getAssignmentsFlow(
    availableAfter: Instant? = null,
    availableBefore: Instant? = null,
    burned: Boolean? = null,
    hidden: Boolean? = null,
    ids: Array<Int>? = null,
    levels: Array<Int>? = null,
    passed: Boolean? = null,
    srsStages: Array<Int>? = null,
    started: Boolean? = null,
    subjectIds: Array<Int>? = null,
    subjectTypes: Array<SubjectType>? = null,
    unlocked: Boolean? = null,
    updatedAfter: Instant? = null,
    firstPageAfterId: Long? = null
) = flowOfResponses(initialPageAfterId = firstPageAfterId) { lastId ->
    getAssignments(
        availableAfter = availableAfter,
        availableBefore = availableBefore,
        burned = burned,
        hidden = hidden,
        ids = ids,
        levels = levels,
        passed = passed,
        srsStages = srsStages,
        started = started,
        subjectIds = subjectIds,
        subjectTypes = subjectTypes,
        unlocked = unlocked,
        updatedAfter = updatedAfter,
        pageAfterId = lastId
    )
}

fun WaniKaniV2.getLevelProgressionsFlow(
    updatedAfter: Instant? = null,
    ids: Array<Int>? = null,
    firstPageAfterId: Long? = null
) = flowOfResponses(initialPageAfterId = firstPageAfterId) { lastId ->
    getLevelProgressions(
        updatedAfter = updatedAfter,
        ids = ids,
        pageAfterId = lastId
    )
}

fun WaniKaniV2.getResetsFlow(
    updatedAfter: Instant? = null,
    ids: Array<Int>? = null,
    firstPageAfterId: Long? = null
) = flowOfResponses(initialPageAfterId = firstPageAfterId) { lastId ->
    getResets(
        updatedAfter = updatedAfter,
        ids = ids,
        pageAfterId = lastId
    )
}

fun WaniKaniV2.getReviewsFlow(
    updatedAfter: Instant? = null,
    ids: Array<Int>? = null,
    subjectIds: Array<Int>? = null,
    assignmentIds: Array<Int>? = null,
    firstPageAfterId: Long? = null
) = flowOfResponses(initialPageAfterId = firstPageAfterId) { pageAfterId ->
    getReviews(
        updatedAfter = updatedAfter,
        ids = ids,
        subjectIds = subjectIds,
        assignmentIds = assignmentIds,
        pageAfterId = pageAfterId
    )
}

fun WaniKaniV2.getReviewStatisticsFlow(
    hidden: Boolean? = null,
    ids: Array<Int>? = null,
    percentagesGreaterThan: Int? = null,
    percentagesLessThan: Int? = null,
    subjectIds: Array<Int>? = null,
    subjectTypes: Array<SubjectType>? = null,
    updatedAfter: Instant? = null,
    firstPageAfterId: Long? = null
) = flowOfResponses(initialPageAfterId = firstPageAfterId) { pageAfterId ->
    getReviewStatistics(
        hidden = hidden,
        ids = ids,
        percentagesGreaterThan = percentagesGreaterThan,
        percentagesLessThan = percentagesLessThan,
        subjectIds = subjectIds,
        updatedAfter = updatedAfter,
        subjectTypes = subjectTypes,
        pageAfterId = pageAfterId
    )
}

fun WaniKaniV2.getStudyMaterialsFlow(
    hidden: Boolean? = null,
    ids: Array<Int>? = null,
    subjectIds: Array<Int>? = null,
    subjectTypes: Array<SubjectType>? = null,
    updatedAfter: Instant? = null,
    firstPageAfterId: Long? = null
) = flowOfResponses(initialPageAfterId = firstPageAfterId) { pageAfterId ->
    getStudyMaterials(
        hidden = hidden,
        ids = ids,
        subjectIds = subjectIds,
        subjectTypes = subjectTypes,
        updatedAfter = updatedAfter,
        pageAfterId = pageAfterId
    )
}

fun WaniKaniV2.getSpacedRepetitionSystemsFlow(
    ids: Array<Int>? = null,
    updatedAfter: Instant? = null,
    firstPageAfterId: Long? = null
) = flowOfResponses(initialPageAfterId = firstPageAfterId) { pageAfterId ->
    getSpacedRepetitionSystems(
        ids = ids,
        updatedAfter = updatedAfter,
        pageAfterId = pageAfterId
    )
}

fun WaniKaniV2.getSubjectsFlow(
    ids: Array<Int>? = null,
    types: Array<SubjectType>? = null,
    slugs: Array<String>? = null,
    levels: Array<Int>? = null,
    hidden: Boolean? = null,
    updatedAfter: Instant? = null,
    firstPageAfterId: Long? = null
) = flowOfResponses(initialPageAfterId = firstPageAfterId) { pageAfterId ->
    getSubjects(
        ids = ids,
        types = types,
        slugs = slugs,
        levels = levels,
        hidden = hidden,
        updatedAfter = updatedAfter,
        pageAfterId = pageAfterId
    )
}

fun WaniKaniV2.getVoiceActorsFlow(
    updatedAfter: Instant? = null,
    ids: Array<Int>? = null,
    firstPageAfterId: Long? = null
) = flowOfResponses(initialPageAfterId = firstPageAfterId) { pageAfterId ->
    getVoiceActors(
        updatedAfter = updatedAfter,
        ids = ids,
        pageAfterId = pageAfterId
    )
}
