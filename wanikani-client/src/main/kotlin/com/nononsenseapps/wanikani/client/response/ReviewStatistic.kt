package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

/**
 * Review statistics summarize the activity recorded in reviews. They contain sum the number of correct and incorrect answers for both meaning and reading. They track current and maximum streaks of correct answers. They store the overall percentage of correct answers versus total answers.
 *
 * A review statistic is created when the user has done their first review on the related subject.
 */
data class ReviewStatistic(
    /**
     * Timestamp when the review statistic was created.
     */
    @Json(name = "created_at")
    val createdAt: Instant,
    /**
     * Unique identifier of the associated subject.
     */
    @Json(name = "subject_id")
    val subjectId: Long,
    /**
     * The type of the associated subject, one of: kanji, radical, or vocabulary.
     */
    @Json(name = "subject_type")
    val subjectType: SubjectType,
    /**
     * Total number of correct answers submitted for the meaning of the associated subject.
     */
    @Json(name = "meaning_correct")
    val meaningCorrect: Int,
    /**
     * Total number of incorrect answers submitted for the meaning of the associated subject.
     */
    @Json(name = "meaning_incorrect")
    val meaningIncorrect: Int,
    /**
     * The longest, uninterrupted series of correct answers ever given for the meaning of the associated subject.
     */
    @Json(name = "meaning_max_streak")
    val meaningMaxStreak: Int,
    /**
     * The current, uninterrupted series of correct answers given for the meaning of the associated subject.
     */
    @Json(name = "meaning_current_streak")
    val meaningCurrentStreak: Int,
    /**
     * Total number of correct answers submitted for the reading of the associated subject.
     */
    @Json(name = "reading_correct")
    val readingCorrect: Int,
    /**
     * Total number of incorrect answers submitted for the reading of the associated subject.
     */
    @Json(name = "reading_incorrect")
    val readingIncorrect: Int,
    /**
     * The longest, uninterrupted series of correct answers ever given for the reading of the associated subject.
     */
    @Json(name = "reading_max_streak")
    val readingMaxStreak: Int,
    /**
     * The current, uninterrupted series of correct answers given for the reading of the associated subject.
     */
    @Json(name = "reading_current_streak")
    val readingCurrentStreak: Int,
    /**
     * The overall correct answer rate by the user for the subject, including both meaning and reading.
     */
    @Json(name = "percentage_correct")
    val percentageCorrect: Int,
    /**
     * Indicates if the associated subject has been hidden, preventing it from appearing in lessons or reviews.
     */
    val hidden: Boolean
): WanikaniData
