package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

data class PronunciationAudioMetadata(
    /**
     * The gender of the voice actor.
     */
    val gender: String,
    /**
     * A unique ID shared between same source pronunciation audio.
     */
    @Json(name = "source_id")
    val sourceId: Long,
    /**
     * Vocabulary being pronounced in kana.
     */
    val pronunciation: String,
    /**
     * A unique ID belonging to the voice actor.
     */
    @Json(name = "voice_actor_id")
    val voiceActorId: Long,
    /**
     * Humanized name of the voice actor.
     */
    @Json(name = "voice_actor_name")
    val voiceActorName: String,
    /**
     * Description of the voice.
     */
    @Json(name = "voice_description")
    val voiceDescription: String
)
