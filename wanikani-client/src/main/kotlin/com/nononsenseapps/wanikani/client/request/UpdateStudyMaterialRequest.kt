package com.nononsenseapps.wanikani.client.request

import com.squareup.moshi.Json

data class UpdateStudyMaterialRequest(
    @Json(name = "study_material")
    val studyMaterial: StudyMaterialUpdateParams
)
