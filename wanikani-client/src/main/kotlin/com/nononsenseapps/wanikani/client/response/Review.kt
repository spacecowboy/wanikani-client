package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

/**
 * Reviews log all the correct and incorrect answers provided through the 'Reviews' section of WaniKani. Review records are created when a user answers all the parts of a subject correctly once; some subjects have both meaning or reading parts, and some only have one or the other. Note that reviews are not created for the quizzes in lessons.
 */
data class Review(
    /**
     * Unique identifier of the associated assignment.
     */
    @Json(name = "assignment_id")
    val assignmentId: Long,
    /**
     * Timestamp when the review was created.
     */
    @Json(name = "created_at")
    val createdAt: Instant,
    /**
     * The SRS stage interval calculated from the number of correct and incorrect answers, with valid values ranging from 1 to 9
     */
    @Json(name = "ending_srs_stage")
    val endingSrsStage: Int? = null,
    /**
     * The number of times the user has answered the meaning incorrectly.
     */
    @Json(name = "incorrect_meaning_answers")
    val incorrectMeaningAnswers: Int,
    /**
     * The number of times the user has answered the reading incorrectly.
     */
    @Json(name = "incorrect_reading_answers")
    val incorrectReadingAnswers: Int,
    /**
     * Unique identifier of the associated spaced_repetition_system
     */
    @Json(name = "spaced_repetition_system_id")
    val spacedRepetitionSystemId: Long,
    /**
     * The starting SRS stage interval, with valid values ranging from 1 to 8
     */
    @Json(name = "starting_srs_stage")
    val startingSrsStage: Int,
    /**
     * Unique identifier of the associated subject.
     */
    @Json(name = "subject_id")
    val subjectId: Long
): WanikaniData
