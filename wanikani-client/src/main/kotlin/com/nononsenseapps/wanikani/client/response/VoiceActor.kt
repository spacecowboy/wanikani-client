package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

/**
 * Available voice actors used for vocabulary reading pronunciation audio.
 */
data class VoiceActor(
    @Json(name = "created_at")
    val createdAt: Instant,
    /**
     * Details about the voice actor.
     */
    val description: String,
    /**
     * male or female
     */
    val gender: String,
    /**
     * The voice actor's name
     */
    val name: String
): WanikaniData

