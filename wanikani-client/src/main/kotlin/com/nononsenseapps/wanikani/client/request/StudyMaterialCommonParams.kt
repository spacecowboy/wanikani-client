package com.nononsenseapps.wanikani.client.request

abstract class StudyMaterialCommonParams {
    /**
     * Meaning notes specific for the subject.
     */
    abstract val meaningNote: String?

    /**
     * Reading notes specific for the subject.
     */
    abstract val readingNote: String?

    /**
     * Meaning synonyms for the subject.
     */
    abstract val meaningSynonyms: List<String>?
}
