package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

/**
 * The summary report contains currently available lessons and reviews and the reviews that will become available in the next 24 hours, grouped by the hour.
 */
data class Summary(
    /**
     * Details about subjects available for lessons.
     */
    val lessons: List<SummaryLesson>,
    /**
     * Earliest date when the reviews are available. Is null when the user has no reviews scheduled.
     */
    @Json(name = "next_reviews_at")
    val nextReviewsAt: Instant?,
    /**
     * Details about subjects available for reviews now and in the next 24 hours by the hour (total of 25 objects).
     */
    val reviews: List<SummaryReview>
): WanikaniData
