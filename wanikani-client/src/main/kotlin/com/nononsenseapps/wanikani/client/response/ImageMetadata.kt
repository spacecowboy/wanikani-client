package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

sealed class ImageMetadata

data class SvgMetadata(
    @Json(name = "inline_styles")
    val inlineStyles: Boolean
): ImageMetadata()

data class PngMetadata(
    val color: String,
    val dimensions: String,
    @Json(name = "style_name")
    val styleName: String
): ImageMetadata()
