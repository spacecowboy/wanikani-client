package com.nononsenseapps.wanikani.client.request

import com.squareup.moshi.Json

data class CreateStudyMaterialRequest(
    @Json(name = "study_material")
    val studyMaterial: StudyMaterialCreateParams
)
