package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

enum class ReadingType {
    @Json(name = "kunyomi")
    KUNYOMI,
    @Json(name = "onyomi")
    ONYOMI,
    @Json(name = "nanori")
    NANORI
}
