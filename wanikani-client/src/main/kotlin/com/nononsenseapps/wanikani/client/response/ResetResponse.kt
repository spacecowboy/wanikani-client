package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.net.URL
import java.time.Instant

data class ResetResponse(
    override val `object`: ObjectType,
    override val url: URL,
    @Json(name = "data_updated_at")
    override val dataUpdatedAt: Instant,
    override val data: Reset,
    val id: Long
): WaniKaniResourceResponse()
