package com.nononsenseapps.wanikani.client.response

data class ContextSentence(
    /** English translation of the sentence */
    val en: String,
    /** Japanese context sentence */
    val ja: String
)
