package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.net.URL
import java.time.Instant

data class ResetsResponse(
    override val pages: Pages,
    @Json(name = "total_count")
    override val totalCount: Int,
    override val data: List<ResetResponse>,
    override val `object`: ObjectType,
    override val url: URL,
    @Json(name = "data_updated_at")
    override val dataUpdatedAt: Instant?
): WaniKaniCollectionResponse<ResetResponse>()
