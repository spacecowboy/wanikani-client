package com.nononsenseapps.wanikani.client.request

import com.squareup.moshi.Json

data class StudyMaterialUpdateParams(
    @Json(name = "meaning_note")
    override val meaningNote: String? = null,
    @Json(name = "reading_note")
    override val readingNote: String? = null,
    @Json(name = "meaning_synonyms")
    override val meaningSynonyms: List<String>? = null
) : StudyMaterialCommonParams()
