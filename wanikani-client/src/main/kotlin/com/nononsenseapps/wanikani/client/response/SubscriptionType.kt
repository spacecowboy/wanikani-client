package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

enum class SubscriptionType {
    @Json(name = "free")
    FREE,
    @Json(name = "recurring")
    RECURRING,
    @Json(name = "lifetime")
    LIFETIME
}
