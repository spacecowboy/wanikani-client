package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json

data class Meaning(
    /**
     * A singular subject meaning.
     */
    val meaning: String,
    /**
     * Indicates priority in the WaniKani system.
     */
    val primary: Boolean,
    /**
     * Indicates if the meaning is used to evaluate user input for correctness.
     */
    @Json(name = "accepted_answer")
    val acceptedAnswer: Boolean
)
