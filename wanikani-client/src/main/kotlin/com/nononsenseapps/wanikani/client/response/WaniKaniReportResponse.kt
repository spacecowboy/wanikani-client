package com.nononsenseapps.wanikani.client.response

abstract class WaniKaniReportResponse : WaniKaniResponse() {
    /**
     * These are the attributes that are specific to that particular instance and kind of resource.
     */
    abstract val data: List<WanikaniData>
}
