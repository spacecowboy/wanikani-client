package com.nononsenseapps.wanikani.client.request

data class UpdateUserParams(
    val preferences: UpdateUserPreferences
)

