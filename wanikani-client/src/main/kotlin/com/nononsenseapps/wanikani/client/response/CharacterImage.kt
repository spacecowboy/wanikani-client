package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.net.URL

sealed class CharacterImage {
    /**
     * The location of the image.
     */
    abstract val url: URL
    /**
     * The content type of the image. Currently the API delivers image/png and image/svg+xml.
     */
    abstract val contentType: ImageContentType
    /**
     * Details about the image. Each content_type returns a uniquely structured object.
     */
    abstract val metadata: ImageMetadata
}

data class CharacterPngImage(
    override val url: URL,
    @Json(name = "content_type")
    override val contentType: ImageContentType,
    override val metadata: PngMetadata
): CharacterImage()

data class CharacterSvgImage(
    override val url: URL,
    @Json(name = "content_type")
    override val contentType: ImageContentType,
    override val metadata: SvgMetadata
): CharacterImage()
