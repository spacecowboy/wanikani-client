package com.nononsenseapps.wanikani.client.response

import com.squareup.moshi.Json
import java.time.Instant

data class SummaryLesson(
    /**
     * When the paired subject_ids are available for lessons. Always beginning of the current hour when the API endpoint is accessed.
     */
    @Json(name = "available_at")
    val availableAt: Instant,
    /**
     * Collection of unique identifiers for subjects.
     */
    @Json(name = "subject_ids")
    val subjectIds: List<Long>
)
