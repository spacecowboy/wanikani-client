import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    `maven-publish`
    id("org.jetbrains.dokka") version "0.10.1"
    signing
}

dependencies {
    val coroutinesVersion = "1.4.1"
    val moshiVersion = "1.11.0"
    val retrofitVersion = "2.9.0"
    val okhttpVersion = "4.7.2"

    api("com.squareup.okhttp3:okhttp:$okhttpVersion")

    implementation(kotlin("stdlib"))
    compileOnly("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")

    implementation("com.squareup.retrofit2:retrofit:$retrofitVersion")
    implementation("com.squareup.retrofit2:converter-moshi:$retrofitVersion")
    implementation("com.squareup.moshi:moshi:$moshiVersion")
    implementation("com.squareup.moshi:moshi-kotlin:$moshiVersion")
    implementation("com.squareup.moshi:moshi-adapters:$moshiVersion")

    testImplementation(kotlin("stdlib"))
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesVersion")
    testImplementation("junit:junit:4.13")
    testImplementation(kotlin("test"))
    testImplementation(kotlin("test-junit"))
    testImplementation("com.squareup.okhttp3:mockwebserver:$okhttpVersion")
}

java {
    withJavadocJar()
    withSourcesJar()
}

tasks {
    withType<KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = "1.8"
    }

    val dokka = named<org.jetbrains.dokka.gradle.DokkaTask>("dokka")

    dokka.configure {
        outputs.cacheIf { true }
        outputFormat = "html"
        outputDirectory = "$buildDir/dokka"
    }

    named("build") {
        dependsOn(dokka)
    }
}

publishing {
    repositories {
        maven {
            name = "localFile"
            url = uri("file://$buildDir/repo")
        }
//        maven {
//            name = "mavenCentral"
//            url = uri("https://oss.sonatype.org/service/local/staging/deploy/maven2")
//            val nexusUsername: String by project
//            val nexusPassword: String by project
//            credentials {
//                username = nexusUsername
//                password = nexusPassword
//            }
//        }
    }
    publications {
        create<MavenPublication>("mavenJava") {
            groupId = "com.nononsenseapps.wanikani"
            artifactId = "wanikani-client"
            version = "${project.version}"

            from(components["java"])
            versionMapping {
                usage("java-api") {
                    fromResolutionOf("runtimeClasspath")
                }
                usage("java-runtime") {
                    fromResolutionResult()
                }
            }

            pom {
                name.set("WaniKani Client")
                description.set("A client implementation of the WaniKani API in Kotlin")
                url.set("https://gitlab.com/spacecowboy/wanikani-client")
//                properties.set(mapOf(
//                    "myProp" to "value",
//                    "prop.with.dots" to "anotherValue"
//                ))
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("http://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("spacecowboy")
                        name.set("Jonas Kalderstam")
                        email.set("jonas@cowboyprogrammer.org")
                    }
                }
                scm {
                    connection.set("scm:git:git://gitlab.com/spacecowboy/wanikani-client.git")
                    developerConnection.set("scm:git:ssh://git@gitlab.com:spacecowboy/wanikani-client.git")
                    url.set("https://gitlab.com/spacecowboy/wanikani-client")
                }
            }
        }
    }
}

signing {
    useGpgCmd()
    sign(publishing.publications.findByName("mavenJava"))
}
