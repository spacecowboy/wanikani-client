# WaniKani Client

This is an implementation of the [WaniKani](https://www.wanikani.com/) API in Kotlin using Retrofit, OkHttp, and coroutines.

See <https://docs.api.wanikani.com/> for official docs.

Please note that while this code is free software - by using WaniKani's API you are bound to their terms and services.

## How to use this in your project?

Just add this dependency to your gradle file:

```
implementation("com.nononsenseapps.wanikani:wanikani-client:<VERSION>")
```

Then you'd do something like this:

```kotlin
fun currentLessonsAndReviews(apiKey: String) {
    val client = getWaniKaniClient(apiKey)

    runBlocking {
        val user = client.getUser().data

        println("Hello ${user.username}, you are level ${user.level}")

        val summary = client.getSummary().data

        val lessonsAvailableNow = summary.lessons
            .filter { it.availableAt < Instant.now() }
            .fold(0) { total: Int, summaryLesson: SummaryLesson ->
                total + summaryLesson.subjectIds.count()
            }

        println("You have $lessonsAvailableNow lessons waiting for you")

        val reviewsAvailableNow = summary.reviews
            .filter { it.availableAt < Instant.now() }
            .fold(0) { total: Int, summaryReview: SummaryReview ->
                total + summaryReview.subjectIds.count()
            }

        println("You have $reviewsAvailableNow reviews waiting for you")

        val moreReviewsAt = summary.nextReviewsAt

        if (moreReviewsAt == null) {
            println("No more reviews after that - you're done!")
        } else {
            println("More reviews coming up at $moreReviewsAt")
        }
    }
}
```

Which would produce output similar to:

```
Hello cowboyprogrammer, you are level 26
You have 48 lessons waiting for you
You have 94 reviews waiting for you
More reviews coming up at 2020-06-13T19:00:00Z
```