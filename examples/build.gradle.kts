import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib"))
    implementation(project(":wanikani-client"))
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.7")
//    implementation("com.squareup.retrofit2:retrofit:2.9.0")
//    implementation("com.squareup.retrofit2:converter-moshi:2.9.0")
//    implementation("com.squareup.moshi:moshi:1.9.2")
//    implementation("com.squareup.moshi:moshi-kotlin:1.9.2")
//    implementation("com.squareup.moshi:moshi-adapters:1.9.2")
//    implementation("com.squareup.okhttp3:okhttp:4.7.2")
//
//    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.7")
//    testImplementation("junit:junit:4.13")
//    testImplementation(kotlin("test"))
//    testImplementation(kotlin("test-junit"))
//    testImplementation("com.squareup.okhttp3:mockwebserver:4.7.2")
}

tasks {
    withType<KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = "1.8"
    }
}