#!/bin/bash

set -u

TARGET="${1:-HEAD}"


current_default="$(git describe --tags --abbrev=0 "${TARGET}")"


echo >&2 -n "Current version [${current_default}]: "
read -r current_in

if [ -z "${current_in}" ]; then
  CURRENT_VERSION="${current_default}"
else
  CURRENT_VERSION="${current_in}"
fi

next_default="$(cat gradle.properties | grep "^version=" | sed "s|version=\(.*\)|\\1|")"
echo >&2 -n "Next version [${next_default}]: "
read -r next_in

if [ -z "${next_in}" ]; then
  NEXT_VERSION="${next_default}"
else
  NEXT_VERSION="${next_in}"
fi

CL="# ${NEXT_VERSION}
$(git shortlog -w76,2,9 --format='* [%h] %s' ${CURRENT_VERSION}..HEAD)
"

tmpfile="$(mktemp)"

echo "${CL}" > "${tmpfile}"

sensible-editor "${tmpfile}"

echo >&2 "${NEXT_VERSION}:"
cat >&2 "${tmpfile}"

read -r -p "Write changelog? [y/N] " response
if [[ "$response" =~ ^[yY]$ ]]
then
  PREV=""
  if [ -f CHANGELOG.md ]; then
    read -r -d '' PREV <CHANGELOG.md
  fi

  cat >CHANGELOG.md <<EOF
$(cat "${tmpfile}")

${PREV}
EOF
fi

read -r -p "Update gradle versions? [y/N] " response
if [[ "$response" =~ ^[yY]$ ]]
then
  sed -i "s|\(version=\).*|\\1${NEXT_VERSION}|" gradle.properties
fi

read -r -p "Commit changes? [y/N] " response
if [[ "$response" =~ ^[yY]$ ]]
then
  git add gradle.properties
  git add CHANGELOG.md
  git commit -m "Releasing ${NEXT_VERSION}"
fi


read -r -p "Make tag? [y/N] " response
if [[ "$response" =~ ^[yY]$ ]]
then
  git tag -asm "$(cat "${tmpfile}")" "${NEXT_VERSION}"
fi

read -r -p "Push commit and tag? [y/N] " response
if [[ "$response" =~ ^[yY]$ ]]
then
  git push --follow-tags
fi

read -r -p "Upload to bintray? [y/N] " response
if [[ "$response" =~ ^[nN]$ ]]
then
  cat <<EOF
Remember to publish the release with

  ./gradlew bintrayUpload -Pupload
EOF
  exit
fi

if [[ $(git diff --stat) != '' ]]; then
  read -r -p "Your working is dirty. Clean it up then press ENTER to continue..." response
fi

if [[ $(git diff --stat) != '' ]]; then
  cat <<EOF
Your working dir is dirty - please clean and run publish manually with

  ./gradlew bintrayUpload -Pupload
EOF
fi

./gradlew publishToSonatype
