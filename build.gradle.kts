buildscript {
    repositories {
        mavenCentral()
        google()
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.10")
    }
}

plugins {
    id("io.github.gradle-nexus.publish-plugin") version "1.0.0"
}

allprojects {
    group = "com.nononsenseapps.wanikani"

    repositories {
        mavenCentral()
        google()
    }
}

tasks.register("clean") {
    doLast {
        delete(rootProject.buildDir)
    }
}

nexusPublishing {
    repositories {
        sonatype {
            val nexusUsername: String by project
            val nexusPassword: String by project
            username.set(nexusUsername)
            password.set(nexusPassword)
        }
    }
}
